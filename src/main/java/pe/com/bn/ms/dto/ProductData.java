package pe.com.bn.ms.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author prov_destrada
 * @created 26/10/2020 - 11:36 a. m.
 * @project ms-dataclients
 */
@Getter
@Setter
public class ProductData implements Serializable
{
    private String productCode;
    private String productDescription;
    private String accountNumber;
    private String accountType;
    private String accountTypeDescription;
    private String addressCoreDescription;
    private String fullNameAccount;
    private String stateAccount;
    private String stateAccountDescription;
    private String accountSituationCode;
    private String accountSituationDescription;
    private String accountingState;
    private String accountingStateDescription;
    private String openingDate;
    private String cancellationDate;
    private String amountAvailable;
    private String flagAccountUOB;
    private String accountCCI;
    private String internalEntityCode;
    private String internalEntityDescription;
    private String accountNature;
    private String accountNatureDescription;
    private String shippingMeansCode;
}
