package pe.com.bn.ms.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @author prov_destrada
 */
@Data
public class GatewayResponse implements Serializable
{
    private String datos;
    private String filler;
    private String longitud;
    private String transId;
    private String mensaje;
    private String msgno;
    private String id;
}
