package pe.com.bn.ms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author prov_destrada
 * @created 16/11/2020 - 05:24 p. m.
 * @project ms-dataclients
 */
@Getter
@Setter
public class CustomerAccountData implements Serializable
{
	//BODY-DATACUENTA
	@ApiModelProperty(value = "") private String fapert;
	@ApiModelProperty(value = "") private String csubcta;
	@ApiModelProperty(value = "") private String cdepend;
	@ApiModelProperty(value = "") private String ccliente;
	@ApiModelProperty(value = "") private String cgrupo;
	@ApiModelProperty(value = "") private String ccuenta;
	@ApiModelProperty(value = "") private String coinCode;
	@ApiModelProperty(value = "") private String numberRUC;
	@ApiModelProperty(value = "") private String typeAper;
	@ApiModelProperty(value = "") private String ccal;
	@ApiModelProperty(value = "") private String tper;
	@ApiModelProperty(value = "") private String nombresCli;
	@ApiModelProperty(value = "") private String direccionCli;
	@ApiModelProperty(value = "") private String acortoCli;
	@ApiModelProperty(value = "") private String nombresGRP;
	@ApiModelProperty(value = "") private String direccionGRP;
	@ApiModelProperty(value = "") private String acortoGRP;
	@ApiModelProperty(value = "") private String asig;
	@ApiModelProperty(value = "") private String cubi;
	@ApiModelProperty(value = "") private String cruc;
	@ApiModelProperty(value = "") private String cres;
	@ApiModelProperty(value = "") private String csec;
	@ApiModelProperty(value = "") private String lele;
	@ApiModelProperty(value = "") private String nreg;
	@ApiModelProperty(value = "") private String csbs;
	@ApiModelProperty(value = "") private String csct;
	@ApiModelProperty(value = "") private String cnac;
	@ApiModelProperty(value = "") private String cact;
	@ApiModelProperty(value = "") private String cext;
	@ApiModelProperty(value = "") private String telf;
	@ApiModelProperty(value = "") private String anex;
	@ApiModelProperty(value = "") private String nfax;
	@ApiModelProperty(value = "") private String filler1;
	@ApiModelProperty(value = "") private String cemp;
	@ApiModelProperty(value = "") private String cpos;
	@ApiModelProperty(value = "") private String ceco;
	@ApiModelProperty(value = "") private String acact;
	@ApiModelProperty(value = "") private String aemp;
	@ApiModelProperty(value = "") private String acontrib;
	@ApiModelProperty(value = "") private String asec;
	@ApiModelProperty(value = "") private String acortoCta2;
	@ApiModelProperty(value = "") private String sfin;
	@ApiModelProperty(value = "") private String filler2;


}
