package pe.com.bn.ms.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author prov_destrada
 * @created 16/11/2020 - 05:24 p. m.
 * @project ms-dataclients
 */
@Getter
@Setter
public class CustomerAccount implements Serializable
{
	//BODY-DATACLIENTECUENTA
    @ApiModelProperty(value = "") private String codeProduct;
    @ApiModelProperty(value = "") private String accountNumber;
    @ApiModelProperty(value = "") private String typeMoney;
    @ApiModelProperty(value = "") private String accountShortName;
    @ApiModelProperty(value = "") private String accountName;
    @ApiModelProperty(value = "") private String itemAddressDom;
    @ApiModelProperty(value = "") private String itemAddressCore;
    @ApiModelProperty(value = "") private String itemAddressWorkCenter;
    @ApiModelProperty(value = "") private String itemAddressFiscal;
    @ApiModelProperty(value = "") private String accountSituationCode;
    @ApiModelProperty(value = "") private String typeAccount;
    @ApiModelProperty(value = "") private String flagAccountUOB;
    @ApiModelProperty(value = "") private String typeUbigee;
    @ApiModelProperty(value = "") private String codeUbigee;
    @ApiModelProperty(value = "") private String customerAccountFill;
    @ApiModelProperty(value = "") private String shippingMeansCode;
    @ApiModelProperty(value = "") private String typeEvent;
    @ApiModelProperty(value = "") private String reasonEvent;
    @ApiModelProperty(value = "") private String creationReasonCode;
    @ApiModelProperty(value = "") private String flagBlacklist;
    @ApiModelProperty(value = "") private String firstAgreementDate;

}
