package pe.com.bn.ms.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

/**
 * @author prov_destrada
 * @created 26/10/2020 - 01:01 p. m.
 * @project ms-dataclients
 */
@Getter
@Setter
@ApiModel(description = "Persona, este es el cliente. ")
public class CustomerSIDG implements Serializable
{
    // ------------------------------------------
    // Headers - INPUT
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdLong;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdTransaction;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdCodeHostResult;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdEsp1;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdPgm;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdEsp2;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdMessageHostResult;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdUser;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdYear;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdMonth;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdDay;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdRequest;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdForma;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdOfficeOpen;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdOfficeProcess;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdSequential;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdType;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdProgram;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdError;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdErrorLote;
    
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdFill;

    // ------------------------------------------
    // Body - INPUT, Datos de cliente
    @ApiModelProperty(value = "") private String customerCic;
    @ApiModelProperty(value = "") private String inputCodeOffice;
    @ApiModelProperty(value = "") private String operationalCodeOffice;
    @ApiModelProperty(value = "") private String typeDocument;
    @ApiModelProperty(value = "") private String numberDocument;
    @ApiModelProperty(value = "") private String flagUnderAge;
    @ApiModelProperty(value = "") private String flagInterdicto;
    @ApiModelProperty(value = "") private String lastName;
    @ApiModelProperty(value = "") private String motherLastName;
    @ApiModelProperty(value = "") private String marriedLastName;
    @ApiModelProperty(value = "") private String firstName;
    @ApiModelProperty(value = "") private String secondName;
    @ApiModelProperty(value = "") private String codeGender;
    @ApiModelProperty(value = "") private String codeCivilStatus;
    @ApiModelProperty(value = "") private String yearsOfStudy;
    @ApiModelProperty(value = "") private String customerHeight;
    @ApiModelProperty(value = "") private String fathersName;
    @ApiModelProperty(value = "") private String mothersName;
    @ApiModelProperty(value = "") private String codeScaleOfInstruction;
    @ApiModelProperty(value = "") private String birthdate;
    @ApiModelProperty(value = "") private String codeInternalCustomer;
    @ApiModelProperty(value = "") private String codeInternalGroup;
    @ApiModelProperty(value = "") private String statusRelationship;
    @ApiModelProperty(value = "") private String numberPhone;
    @ApiModelProperty(value = "") private String codeCustomerEmployment;
    @ApiModelProperty(value = "") private String codeEntityCustomer;
    @ApiModelProperty(value = "") private String customerFill;

    // ------------------------------------------
    // Body OUTPUT
    private CustomerAddress       customerAddress;
    private CustomerAccount       customerAccount;
    private CustomerAccountData customerAccountData;

}
