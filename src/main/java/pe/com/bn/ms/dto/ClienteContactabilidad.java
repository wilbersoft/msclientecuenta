package pe.com.bn.ms.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Referencias de un cliente. ")
public class ClienteContactabilidad implements Serializable
{
    // Headers
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdLong;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdTran;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdTimestamp;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdCodTran;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdCodUser;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdCodRet;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdDesRet;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdCodCanal;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdCodTerm;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdCodRetLog;
    @ApiModelProperty(value = "") @JsonProperty(access = WRITE_ONLY) private String hdDesRetLog;

    // Body
    @JsonProperty(access = WRITE_ONLY) private String cic;
    @JsonProperty(access = WRITE_ONLY) private String accion; //Campo para el host (C:Consulta - A:Actualizar)

    @ApiModelProperty(value = "Tipo de Documento de Identidad") private String documentType;
    @ApiModelProperty(value = "") private                               String documentNumber;
    @ApiModelProperty(value = "") private                               String fullName;
    @ApiModelProperty(value = "") private                               String operatorMobile;
    @ApiModelProperty(value = "") private                               String numberMobile;
    @ApiModelProperty(value = "") private                               String homePhone;
    @ApiModelProperty(value = "") private                               String workingPhone;
    @ApiModelProperty(value = "") private                               String annexPhone;
    @ApiModelProperty(value = "") private                               String personalEmail;
    @ApiModelProperty(value = "") private                               String workEmail;

    public ClienteContactabilidad(String hdLong, String hdTran, String hdCodTran, String cic, String accion)
    throws Exception
    {
        this.hdLong      = hdLong;
        this.hdTran      = hdTran;
        this.hdTimestamp = createInstantDate();
        this.hdCodTran   = hdCodTran;
        this.cic         = cic;
        this.accion      = accion;

    }

    // Custom getters and setters

    public void setNumberMobile(String numberMobile)
    {
        gtOperatorMobile(numberMobile);
        this.numberMobile = numberMobile;
    }

    private void gtOperatorMobile(String numberMobile)
    {
        this.operatorMobile = numberMobile.substring(0, 1);
    }
    
    // Métodos

    private String createInstantDate()
    {
        Calendar         calendario     = GregorianCalendar.getInstance();
        Date             fecha          = calendario.getTime();
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("yyyy-MM-dd-HH-m-s-999999");
        return formatoDeFecha.format(fecha);
    }
}
