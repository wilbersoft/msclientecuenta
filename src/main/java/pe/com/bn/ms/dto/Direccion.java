package pe.com.bn.ms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Direccion de un cliente. ")
public class Direccion implements Serializable
{
    @ApiModelProperty(name = "ItemAddress", value = "Numero Interno") private                  String itemAddress;
    @ApiModelProperty(name = "TypeAddress", value = "Item Direccion") private                  String typeAddress;
    @ApiModelProperty(name = "TypeRoad", value = "Codigo Tipo de Direccion") private           String typeRoad;
    @ApiModelProperty(name = "DescriptionRoad", value = "Codigo Tipo de Via") private          String descriptionRoad;
    @ApiModelProperty(name = "AddressNumber", value = "Descripcion Via") private               String addressNumber;
    @ApiModelProperty(name = "BlockNumber", value = "Direccion Numero") private                String blockNumber;
    @ApiModelProperty(name = "SquareNumber", value = "Direccion Bloque") private               String squareNumber;
    @ApiModelProperty(name = "LotNumber", value = "Direccion Manzana") private                 String lotNumber;
    @ApiModelProperty(name = "FlatNumber", value = "Direccion Lote") private                   String flatNumber;
    @ApiModelProperty(name = "InteriorDptNumber", value = "Direccion Piso") private            String interiorDptNumber;
    @ApiModelProperty(name = "ReportedAddress", value = "Direccion Dpto/Interior") private     String reportedAddress;
    @ApiModelProperty(name = "LocationReference", value = "Direccion Reportada") private       String locationReference;
    @ApiModelProperty(name = "PostOfficeBox", value = "Referencia Ubicacion") private          String postOfficeBox;
    @ApiModelProperty(name = "AddressPhoneNumber",
                      value = "Apartado Postal") private                                       String addressPhoneNumber;
    @ApiModelProperty(name = "AddressAnnexPhone", value = "Telefono Numero") private           String addressAnnexPhone;
    @ApiModelProperty(name = "AddressFaxPhone", value = "Telefono Anexo") private              String addressFaxPhone;
    @ApiModelProperty(name = "UbigeoType", value = "Fax Numero") private                       String ubigeoType;
    @ApiModelProperty(name = "UbigeoCode", value = "Origen del Ubigeo") private                String ubigeoCode;
    @ApiModelProperty(name = "FlagDomiciledVisit",
                      value = "Codigo Ubigeo") private                                         String flagDomiciledVisit;
    @ApiModelProperty(name = "DomiciledVisitDate",
                      value = "Indicador de Visita S/N") private                               String domiciledVisitDate;
    @ApiModelProperty(name = "HomeVisitDescription",
                      value = "Fecha de Visita") private                                       String homeVisitDescription;
    @ApiModelProperty(name = "ZipCode", value = "Resultado de la Visita") private              String zipCode;
    @ApiModelProperty(name = "UbigeoAhorrosCode", value = "Codigo Postal") private             String ubigeoAhorrosCode;
    @ApiModelProperty(name = "UbigeoCtaCteCode", value = "Cubigeo ahorros") private            String ubigeoCtaCteCode;
    @ApiModelProperty(name = "UbigeoReniecCode", value = "Cubigeo cuentas corrientes") private String ubigeoReniecCode;
    @ApiModelProperty(name = "UbigeoSunatCode", value = "Cubigeo RENIEC") private              String ubigeoSunatCode;
    @ApiModelProperty(name = "InputDate", value = "Cubigeo SUNAT") private                     String inputDate;
    @ApiModelProperty(name = "LastChangeDate", value = "Fecha de ingreso") private             String lastChangeDate;
    @ApiModelProperty(name = "StateAddress", value = "Fecha de ultima modificación") private   String stateAddress;
}
