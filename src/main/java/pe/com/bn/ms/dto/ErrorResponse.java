package pe.com.bn.ms.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse implements Serializable
{
    private String  type;
    private Integer httpStatus;
    private String  uri;
    private String  messageLocalized;
    private Object  parameters;
    private String  msgError;
}
