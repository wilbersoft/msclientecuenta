package pe.com.bn.ms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

@Setter
@Getter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "Persona natural, es hijo de PersonaCliente. ")
public class PersonaNatural implements Serializable
{
    @ApiModelProperty(name = "LastName01", value = "Apellido Paterno") private               String lastName01;
    @ApiModelProperty(name = "LastName02", value = "Apellido Materno") private               String lastName02;
    @ApiModelProperty(name = "MarriedName", value = "Apellidos de Casada") private           String marriedName;
    @ApiModelProperty(name = "FirstName", value = "Nombre Primero") private                  String firstName;
    @ApiModelProperty(name = "SecondName", value = "Nombre Segundo") private                 String secondName;
    @ApiModelProperty(name = "CountryOfBirthCode",
                      value = "Pais Nacionalidad") private                                   String countryOfBirthCode;
    @ApiModelProperty(name = "BirthDate", value = "Fecha  Nacimiento") private               String birthDate;
    @ApiModelProperty(name = "UbigeoBirthType",
                      value = "Origen del Ubigeo") private                                   String ubigeoBirthType;
    @ApiModelProperty(name = "UbigeoBirthCode",
                      value = "Codigo Ubigeo") private                                       String ubigeoBirthCode;
    @ApiModelProperty(name = "SexCode", value = "Codigo de Sexo") private                    String sexCode;
    @ApiModelProperty(name = "CivilStatusCode",
                      value = "Codigo Estado Civil") private                                 String civilStatusCode;
    @ApiModelProperty(name = "NumberMobile", value = "Telefono Personal") private            String numberMobile;
    @ApiModelProperty(name = "PersonalEmail",
                      value = "eMail Personal") private                                      String personalEmail;
    @ApiModelProperty(name = "WorkEmail", value = "eMail Trabajo") private                   String workEmail;
    @ApiModelProperty(name = "DegreeOfInstructionCode",
                      value = "Codigo Grado Instruccion") private                            String degreeOfInstructionCode;
    @ApiModelProperty(name = "ProfessionCode",
                      value = "Codigo Profesion") private                                    String professionCode;
    @ApiModelProperty(name = "OccupationCode",
                      value = "Codigo Ocupacion") private                                    String occupationCode;
    @ApiModelProperty(name = "OccupationCodeReported",
                      value = "Ocupacion Reportada") private                                 String occupationCodeReported;
    @ApiModelProperty(name = "FlagBusinessPerson",
                      value = "Indicador Persona con Negocio") private                       String flagBusinessPerson;
    @ApiModelProperty(name = "NumberRUC", value = "RUC Persona Natural con Negocio") private String numberRUC;
    @ApiModelProperty(name = "DependentNum", value = "Numero de Dependientes") private       String dependentNum;
    @ApiModelProperty(name = "LocalityBirth",
                      value = "Ubigeo Nacimiento Localidad") private                         String localityBirth;
    @ApiModelProperty(name = "YearOfStudy", value = "Años de Estudios Reniec") private       String yearOfStudy;
    @ApiModelProperty(name = "HeightClient", value = "Estatura Reniec") private              String heightClient;
    @ApiModelProperty(name = "FatherName", value = "Nombres del Padre Reniec") private       String fatherName;
    @ApiModelProperty(name = "MotherName", value = "Nombres de la Madre Reniec") private     String motherName;
}
