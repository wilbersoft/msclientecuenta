package pe.com.bn.ms.tracert.domain;

import lombok.Getter;
import lombok.Setter;

import static pe.com.bn.ms.util.enums.MessagesError.HANDLE_THROWABLE;

/**
 * @author prov_destrada
 * @created 30/10/2020 - 11:40 a. m.
 * @project ms-dataclients
 */
@Getter
@Setter
public class ExceptionFields
{
    private String errorCode;
    private String friendlyMessage;
    private String technicalMessage;
    private Object parameters;

    public ExceptionFields()
    {
        this.setErrorCode(HANDLE_THROWABLE.getCodResult());
        this.friendlyMessage  = HANDLE_THROWABLE.getMsg();
        this.technicalMessage = HANDLE_THROWABLE.getMsgError();
    }
}
