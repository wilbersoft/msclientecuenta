package pe.com.bn.ms.tracert.logger;

import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * @author prov_destrada
 * @created 26/10/2020 - 01:01 p. m.
 * @project ms-dataclients
 */
@Log4j2
@Component
public class LoggerServiceImpl implements LoggerService
{
    /**
     * =================================================================================================================
     * Intercepta los métodos durante su ejecución y traza el tiempo que demora desde que inicia hasta que finaliza el
     * método
     *
     * @param joinPoint contiene todos los objetos usados como parámetros y son enviados a través del método
     * @param uuid      uuid
     * @param response  resultado después de finalizar la operación
     * @return response
     */
    @Override
    public Object aroundLoggerTime(ProceedingJoinPoint joinPoint, UUID uuid, boolean response) throws Throwable
    {
        Logger logger   = LoggerFactory.getLogger(joinPoint.getTarget().getClass());
        Object result;
        long   currTime = System.currentTimeMillis();
        logger.info("tx[" + uuid + "] --- INICIO DEL REQUEST ---");
        String metodo = "tx[" + uuid + "] - " + joinPoint.getSignature().getName();
        logger.info(metodo + "()");

        if (!joinPoint.getSignature().getName().equals("init"))
            if (joinPoint.getArgs().length > 0)
                logger.info(metodo + "() INPUT:" + Arrays.toString(joinPoint.getArgs()));

        // ---------------------------------------------------
        // Si se produce un error mientras se leen los objetos
        result = joinPoint.proceed();
        // ---------------------------------------------------

        logger.info("{}(): tiempo transcurrido {} ms.", metodo, (System.currentTimeMillis() - currTime));
        if (result != null && response)
            logger.info("{}(): OUTPUT: {} ms.", metodo, result.toString());

        logger.info("tx[" + uuid + "] --- FIN DEL REQUEST ---");
        return result;
    }

    /**
     * =================================================================================================================
     * Intercepta los métodos durante su ejecución y traza el tiempo que demora desde que inicia hasta que finaliza el
     * método, este método solo se usa para trazar a nivel de service layer
     *
     * @param joinPoint contiene todos los objetos usados como parámetros y son enviados a través del método
     * @param uuid      uuid
     * @param response  resultado después de finalizar la operación
     * @return response
     */
    @Override
    public Object aroundLogger(ProceedingJoinPoint joinPoint, UUID uuid, boolean response) throws Throwable
    {
        Object result;
        long   currTime = System.currentTimeMillis();
        Logger logger   = LoggerFactory.getLogger(joinPoint.getTarget().getClass());
        String metodo   = "tx[" + uuid + "] - " + joinPoint.getSignature().getName();
        logger.info(metodo + "()");

        if (joinPoint.getArgs().length > 0)
            logger.info(metodo + "() INPUT:" + Arrays.toString(joinPoint.getArgs()));

        // ---------------------------------------------------
        // Si se produce un error mientras se leen los objetos
        result = joinPoint.proceed();
        // ---------------------------------------------------

        logger.info(metodo + "(): tiempo transcurrido " + (System.currentTimeMillis() - currTime) + " ms.");

        return result;
    }

    /**
     * =================================================================================================================
     * Intercepta el método luego de haberse producido una excepción, no se deben auditar los errores que se produzcan
     * en los métodos 'aroundLoggerTime' y 'aroundLogger'
     *
     * @param joinPoint contiene todos los objetos usados como parámetros y son enviados a través del método
     * @param uuid      uuid
     * @param ex        excepción que se ha producido
     */
    @Override
    public void afterThrowableLogger(JoinPoint joinPoint, UUID uuid, Throwable ex)
    {
        List<String> methodsFilter = Arrays.asList("aroundLoggerTime", "aroundLogger");
        String       method        = joinPoint.getSignature().getName();

        // Filtrar los métodos que no deben auditarse
        boolean filter = methodsFilter
                .stream()
                .noneMatch(q -> q.equals(method));

        if (filter) {
            long   currTime = System.currentTimeMillis();
            Logger logger   = LoggerFactory.getLogger(joinPoint.getTarget().getClass());
            logger.error("tx[{}] --- INICIO DEL ERROR ---", uuid);
            String metodo = "tx[" + uuid + "] - " + joinPoint.getSignature().getName();
            String target = "tx[" + uuid + "] - " + joinPoint.getTarget();
            logger.error(metodo + "()");

            if (joinPoint.getArgs().length > 0)
                logger.error(metodo + "() INPUT:" + Arrays.toString(joinPoint.getArgs()));

            logger.error("tx[{}] El error se produjo en : {}", uuid, target);
            logger.error("tx[{}] El error fue causado por : {1}", uuid, ex.getCause());
            logger.error("tx[{}] Mensaje de error : {}", uuid, ex.getMessage());
            logger.info("{}(): tiempo transcurrido {} ms.", metodo, (System.currentTimeMillis() - currTime));
            logger.error("tx[{}] --- FIN DEL ERROR ---", uuid);
        }
    }
}
