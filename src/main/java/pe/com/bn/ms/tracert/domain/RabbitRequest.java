package pe.com.bn.ms.tracert.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author prov_destrada
 * @description envía bitácoras (procesos OK)
 */
@Getter
@Setter
@ToString
public class RabbitRequest implements Serializable
{
    // Headers
    private String id;
    private String canal;
    private String ip;
    private String aplicacion;

    private String           collection;
    private RabbitObjectData datos;

}
