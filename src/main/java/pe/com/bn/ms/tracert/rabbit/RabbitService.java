package pe.com.bn.ms.tracert.rabbit;

import org.aspectj.lang.JoinPoint;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

public interface RabbitService
{

    void sendRabbitQueueResponse(JoinPoint joinPoint, Object result, HttpServletRequest httpRequest, UUID uuid);

    void sendRabbitQueueResponseEx(JoinPoint joinPoint, UUID uuid, HttpServletRequest httpRequest, Throwable ex)
    throws Throwable;

}
