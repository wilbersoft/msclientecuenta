package pe.com.bn.ms.tracert.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author prov_destrada
 * @description objeto que contiene los parámetros para auditoría
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RabbitObjectData implements Serializable
{
    private Object input;
    private Object output;
    private String url;
    private String method;
}
