package pe.com.bn.ms.tracert;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

/**
 * @author prov_destrada
 * @created 26/10/2020 - 01:01 p. m.
 * @project ms-dataclients
 */
@Aspect
public class SystemArchitecture
{

    @Pointcut("execution(* pe.com.bn.ms.api.*Api*.*(..))")
    public void inWebLayer()
    {

    }

    @Pointcut("execution(* pe.com.bn.ms.service.*ServiceImpl*.*(..)) ")
    public void inBusinessServiceLayer()
    {

    }

//    @Pointcut("execution(* pe.com.bn.ms.config.datasource.DatasourceConfig.dataSourceProperties()) ")
//    public void inDatasourceProperties()
//    {
//
//    }

//    @Pointcut("execution(* pe.com.bn.ms.config.datasource.DatasourceContextConfig.entityManagerFactoryBean(..)) ")
//    public void validStringConnection()
//    {
//
//    }

}
