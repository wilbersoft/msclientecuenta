package pe.com.bn.ms.tracert.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author prov_destrada
 * @description envía eventos (procesos ERROR)
 */
@Getter
@Setter
@ToString
public class RabbitRequestEx implements Serializable
{
    private String index;
    private String subTipo;
    private String codError;
    private String desError;
    private String programa;
    private String levelError;
    private String tipoOperacion;

    // Headers
    private String canal;
    private String ip;
    private String aplicacion;


}
