package pe.com.bn.ms.tracert.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * Plantilla de elementos que serán enviados por rabbit
 *
 * @author prov_destrada
 */
@Getter
@Setter
@ToString
public class RabbitPlot implements Serializable
{
    private String responseCode;
    private String errorMessage;
    private String channel;
    private String application;
    private String ip;
    private String operatorMobile;
    private String numberMobile;
    private String typeTrans;
    private String typeOp;

    private RabbitObjectData data = new RabbitObjectData();
}
