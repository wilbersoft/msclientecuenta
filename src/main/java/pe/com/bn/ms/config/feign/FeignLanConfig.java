package pe.com.bn.ms.config.feign;

import feign.Logger;
import feign.Request;
import feign.Retryer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

/**
 * Configuración de Feign para servicios en red NAT
 */
@Slf4j
public class FeignLanConfig
{
    @Bean
    public Logger.Level configureLogLevel()
    {
        return Logger.Level.HEADERS;
    }

    @Bean
    public Request.Options timeoutConfiguration()
    {
        return new Request.Options(5000, 30000);
    }

    @Bean
    public Retryer retryer()
    {
        return new Retryer.Default(1L, 150L, 3);
    }
}
