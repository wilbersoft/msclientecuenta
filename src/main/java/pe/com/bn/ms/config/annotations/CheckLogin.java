package pe.com.bn.ms.config.annotations;//package pe.com.bn.ms.config.annotations;
//
//import javax.validation.Constraint;
//import javax.validation.Payload;
//import java.lang.annotation.Retention;
//import java.lang.annotation.RetentionPolicy;
//import java.lang.annotation.Target;
//
//import static java.lang.annotation.ElementType.*;
//
//@Constraint(validatedBy = {CheckLoginValidation.class})
//@Target({METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER})
//@Retention(RetentionPolicy.RUNTIME)
//public @interface CheckLogin {
//
//    String message() default "Validación de forma incorrecta";
//
//    Class<?>[] groups() default {};
//
//    Class<? extends Payload>[] payload() default {};
//
//}
