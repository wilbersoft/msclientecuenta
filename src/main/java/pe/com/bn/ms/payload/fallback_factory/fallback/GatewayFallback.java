package pe.com.bn.ms.payload.fallback_factory.fallback;

import feign.FeignException;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import pe.com.bn.ms.dto.ErrorResponse;
import pe.com.bn.ms.dto.GatewayRequest;
import pe.com.bn.ms.dto.GatewayResponse;
import pe.com.bn.ms.exception.base4XX.FeignHttpException;
import pe.com.bn.ms.payload.GatewayClient;

/**
 * @author prov_destrada
 * @created 14/10/2020 - 05:09 p. m.
 * @project MSDataClients
 */
@Log4j2
@AllArgsConstructor
@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class GatewayFallback implements GatewayClient
{
    private final Throwable cause;

    @Override
    public ResponseEntity<GatewayResponse> runGatewayOperation(HttpHeaders httpHeaders, GatewayRequest request)
    {
        log.error("=================================================================================");
        log.error("Se ha producido un error mientras se consultaba el cliente gateway...");

        ErrorResponse error   = new ErrorResponse();
        String        message = cause.getLocalizedMessage();

        // Verificar que el error haya sido causado por feign
        if (cause instanceof FeignException) {

            int status = ((FeignException) cause).status();

            if (status == 404) {
                log.error("No se puede conectar con el servicio...");
                message = "No se puede conectar con el servicio...";
            }

            error.setType("FeignException");
            error.setHttpStatus(status);
        }
        //
        else {
            error.setType("Throwable");
            error.setHttpStatus(null);
        }

        error.setMessageLocalized(message);
        error.setUri(null);
        error.setParameters(null);

        log.error("=================================================================================");

        throw new FeignHttpException(error);
    }
}
