package pe.com.bn.ms.payload;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import pe.com.bn.ms.config.feign.FeignLanConfig;
import pe.com.bn.ms.dto.GatewayRequest;
import pe.com.bn.ms.dto.GatewayResponse;
import pe.com.bn.ms.payload.fallback_factory.GatewayFallbackFactory;

/**
 * @author prov_destrada
 * @created 14/10/2020 - 04:48 p. m.
 * @project MSDataClients
 */
/*
@FeignClient(name = "gateway-client",
             url = "${bn.client.gate.url}",
             path = "${bn.client.gate.path}",
             configuration = FeignLanConfig.class,
             fallbackFactory = GatewayFallbackFactory.class)
*/
@FeignClient(name = "gateway-client",
			 url = "${bn.service.gate.url}",
			 path = "${bn.service.gate.path}",
			 configuration = FeignLanConfig.class,
			 fallbackFactory = GatewayFallbackFactory.class)
public interface GatewayClient
{
    /**
     * Realiza consulta HOST mediante api gateway
     *
     * @param httpHeaders cabeceras de la aplicación
     * @param request     {@link GatewayRequest}
     * @return {@link GatewayResponse}
     */
    @PostMapping(name = "GatewayRest()",
                 value = "",
                 consumes = MediaType.APPLICATION_JSON_VALUE,
                 produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<GatewayResponse> runGatewayOperation(@RequestHeader HttpHeaders httpHeaders,
                                                        @RequestBody GatewayRequest request);
}
