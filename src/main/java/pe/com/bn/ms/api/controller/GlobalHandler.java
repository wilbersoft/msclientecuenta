package pe.com.bn.ms.api.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.dto.ErrorResponse;
import pe.com.bn.ms.exception.EndpointException;
import pe.com.bn.ms.exception.GenericException;

import static org.springframework.http.HttpStatus.OK;
import static pe.com.bn.ms.util.enums.MessagesError.HANDLE_THROWABLE;

@ControllerAdvice
@RestController
public class GlobalHandler extends GlobalExceptionHandler5XX
{
    /**
     * =================================================================================================================
     * Excepción genérica
     *
     * @param ex      excepción personalizada
     * @param request interfaz genérica web
     * @return {@link ResponseEntity}
     */
    @ExceptionHandler(GenericException.class)
    public final ResponseEntity<?> handleGenericException(GenericException ex, WebRequest request)
    {
        GenericResponse<?> genericResponse = new GenericResponse<>(
                ex.getCodResult(),
                new ErrorResponse(ex.getClass().toString(),
                                  null,
                                  request.getDescription(false),
                                  ex.getLocalizedMessage(),
                                  ex.getParameters(),
                                  ex.getMsgError()),
                				 
                ex.getMsg(),
                ex.getMsgError()
        );

        return new ResponseEntity<>(genericResponse, OK);
    }

    /**
     * =================================================================================================================
     * Excepción genérica
     *
     * @param ex      {@link Throwable}
     * @param request interfaz genérica web
     * @return {@link ResponseEntity}
     */
    @ExceptionHandler(Throwable.class)
    public final ResponseEntity<?> handleAllThrowable(Throwable ex, WebRequest request)
    {
        GenericResponse<?> genericResponse = new GenericResponse<>(
                HANDLE_THROWABLE.getCodResult(),
                new ErrorResponse(ex.getClass().toString(),
                                  null,
                                  request.getDescription(false),
                                  ex.getLocalizedMessage(),
                                  "",
                                  ""),
                HANDLE_THROWABLE.getMsg(),
                HANDLE_THROWABLE.getMsgError()
        );

        return new ResponseEntity<>(genericResponse, OK);
    }

    /**
     * =================================================================================================================
     * Pivoteo de errores
     *
     * @param ex      excepción personalizada
     * @param request interfaz genérica web
     * @return {@link ResponseEntity}
     */
    @ExceptionHandler(EndpointException.class)
    public final ResponseEntity<?> handleEndpointException(EndpointException ex, WebRequest request)
    {
        return new ResponseEntity<>(ex.getGenericResponse(), OK);
    }

}
