package pe.com.bn.ms.api;

import io.swagger.annotations.Api;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.domain.ParameterAddressRequest;
import pe.com.bn.ms.domain.TypeSearchCustomer;
import pe.com.bn.ms.domain.TypeSearchCustomerContactability;
import pe.com.bn.ms.domain.response.DataClient.CustomResponse;
import pe.com.bn.ms.dto.Direccion;
import pe.com.bn.ms.exception.base4XX.TypeSearchNotFoundException;
import pe.com.bn.ms.model.ClientSwg;
import pe.com.bn.ms.model.ClienteContactabilidadSwg;
import pe.com.bn.ms.model.DireccionSwg;
import pe.com.bn.ms.model.PersonaClienteSwg;
import pe.com.bn.ms.service.address.AddressService;
import pe.com.bn.ms.service.customer.CustomerReadService;
import pe.com.bn.ms.util.Shared.DataUtil;
import pe.com.bn.ms.util.ValidacionEntrada;

import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_CIC;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_NUMBER_DOCUMENT;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_NUMBER_PRODUCT;

import java.util.List;
import java.util.ArrayList;

/**
 * @author prov_destrada
 * @description Api solo para consultas (R -> READ)
 */
@RestController
@RequestMapping("/msdataclients")
@Api(tags = "Apis de consulta - DataClients",
     value = "Consulta de clientes y derivados. ")
public class CustomerReadApi
{
    private final CustomerReadService customerService;
    private final AddressService      addressService;

    @Autowired
    public CustomerReadApi(CustomerReadService customerService, AddressService addressService)
    {
        this.customerService = customerService;
        this.addressService  = addressService;
    }

    public boolean validarLongitud(String texto) {
        int longitud = texto.length();
        return longitud >= 2 && longitud <= 12;
    }
    /**
     * =================================================================================================================
     * Descripción: Servicio de consulta para: - Consulta de Clientes por cic -
     * Consulta de Clientes por Tipo y Num Doc - Consultar una dirección de Cliente
     * por cic y numero correlativo
     * <p>
     * Ejemplo: requiere
     *
     * @param cic                código interno cliente
     * @param tipDoc             tipo de documento
     * @param numDoc             número de documento
     * @param typeProduct        tipo de producto
     * @param numProduct         número de producto
     * @param typeSearch         tipo de búsqueda
     * @param flagEntidadExterna flag para consultar la persona desde SUNAT o RENIEC
     * @return GenericResponse<PersonaCliente>
     */
    @ApiOperation(value = "Consulta de Clientes por: cic, typeNumDoc, typeNumProduct. ",
                  response = PersonaClienteSwg.class)
    @GetMapping(value = {"/client/{typeSearch}"},
                produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCustomerInformation(

            @ApiParam(value = "Tipo de búsqueda") @PathVariable String typeSearch,
            @ApiParam(value = "Código interno cliente") @RequestParam(required = false) String cic,
            @ApiParam(value = "Tipo de documento") @RequestParam(required = false) String tipDoc,
            @ApiParam(value = "Número de documento") @RequestParam(required = false) String numDoc,

            @ApiParam(value = "Tipo de producto") @RequestParam(required = false) String typeProduct,
            @ApiParam(value = "Número de producto") @RequestParam(required = false) String numProduct,

            @ApiParam(value = "Flag para consultar la persona desde SUNAT o RENIEC")
            @RequestParam(required = false) String flagEntidadExterna,
            @RequestHeader HttpHeaders httpHeaders)
    throws Throwable
    {
        typeSearch = typeSearch.toUpperCase();

        TypeSearchCustomer clientRequest = new TypeSearchCustomer(typeSearch, cic, tipDoc, numDoc, typeProduct,
                                                                   numProduct, flagEntidadExterna, "");

        GenericResponse<?> genericResponse;

        // Realizar consulta según el tipo de búsqueda
        if (typeSearch.equals(SEARCH_CIC.getType())) {
            //validación de entrada cic
            //Caso error: CIC en blanco
            if(cic==null || cic.trim().length()<=0){
                genericResponse=new GenericResponse();
                genericResponse.setCodResult("99999");
                genericResponse.setMsg("Error: El CIC esta en Blanco");
                genericResponse.setMsgError("");
                return new ResponseEntity<>(genericResponse, HttpStatus.OK);
            }
            //Caso validacion:min=2.max 12
            if(!validarLongitud(cic)){
                genericResponse=new GenericResponse();
                genericResponse.setCodResult("99999");
                genericResponse.setMsg("Error: El CIC debe ser Min 2 y Max 12");
                genericResponse.setMsgError("");
                return new ResponseEntity<>(genericResponse, HttpStatus.OK);
            }
            //Caso Completar ceros a la derecha
            cic=new ValidacionEntrada().completarCerosDerecha(cic,12);
            //try{
                genericResponse = customerService
                        .findCustomerByCic(httpHeaders, clientRequest);
            /*}catch(Exception e){
                // Caso error: Cliente con error en sus datos (no se puede desocmponer la trama)
                genericResponse=new GenericResponse();
                genericResponse.setCodResult("99999");
                genericResponse.setMsg("Cliente con Error en sus datos ");
                genericResponse.setMsgError(" No se puede descomponer Trama Error: "+e.getMessage());
            }*/

        }

        else if (typeSearch.equals(SEARCH_NUMBER_DOCUMENT.getType())) {

            // Caso error: Tipo de Cliente en blanco o con un valor qué no corresponde
            if (tipDoc == null || tipDoc.trim().length() <= 0) {
                genericResponse = new GenericResponse();
                genericResponse.setCodResult("99999");
                genericResponse.setMsg("Error: El Tipo esta en Blanco");
                genericResponse.setMsgError("");
                return new ResponseEntity<>(genericResponse, HttpStatus.OK);
            }
            // Caso validacion:lista= A, B,1,3, 4,5,6.
                //A, B, 3, 4,5= Alfanumerico
                //1, 6 = numerico
            if(!new ValidacionEntrada().validaTipoDocumentoEnLista(tipDoc.toCharArray())){
                genericResponse=new GenericResponse();
                genericResponse.setCodResult("99999");
                genericResponse.setMsg("Error: Tipo documento no esta la lista : A, B,1,3, 4,5,6");
                genericResponse.setMsgError("");
                return new ResponseEntity<>(genericResponse, HttpStatus.OK);
            }
            //Validacion numDoc=min= 3. max=12.
            if(!new ValidacionEntrada().validarLongitudNumeroDocumento(numDoc)){
                genericResponse=new GenericResponse();
                genericResponse.setCodResult("99999");
                genericResponse.setMsg("Error: El Numero Documento debe ser Min 3 y Max 12");
                genericResponse.setMsgError("");
                return new ResponseEntity<>(genericResponse, HttpStatus.OK);
            }
            //Caso : Completar 0 a la izquierda
            numDoc=new ValidacionEntrada().completarCerosIzquierda(numDoc,12);
            try{
                genericResponse = customerService.findCustomerByTypeAndNumberDocument(httpHeaders, clientRequest);
            }catch(Exception e){
                // Caso error: Cliente con error en sus datos (no se puede desocmponer la trama)
                genericResponse=new GenericResponse();
                genericResponse.setCodResult("99999");
                genericResponse.setMsg("Cliente con Error en sus datos ");
                genericResponse.setMsgError(" No se puede descomponer Trama Error: "+e.getMessage());
            }

        }

        //
        else if (typeSearch.equals(SEARCH_NUMBER_PRODUCT.getType())) {

            genericResponse = customerService.findCustomerByTypeAndNumberProduct(httpHeaders, clientRequest);

        }
        // Error
        else {
            throw new TypeSearchNotFoundException("TypeSearch: " + typeSearch);
        }

        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }

    /**
     * =================================================================================================================
     * Descripción: Servicio de consulta para: - Lista de Direcciones de un cliente
     * - Consultar una direccion de Cliente por CIC y numero correlativo
     *
     * @param CIC      código interno de cliente
     * @param numCorre número correlativo
     * @return GenericResponse<Direccion>
     */
    @ApiOperation(value = "Lista de Direcciones de un cliente. ",
                  response = DireccionSwg.class)
    @GetMapping(value = {"/address/{CIC}", "/address/{CIC}/{numCorre}"},
                produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getClientAddress(
            @ApiParam(value = "Código interno de cliente") @PathVariable String CIC,
            @ApiParam(value = "Número correlativo") @PathVariable(required = false) String numCorre)
    throws Throwable
    {
        ParameterAddressRequest addressRequest  = new ParameterAddressRequest(CIC, numCorre);
        GenericResponse         genericResponse = new GenericResponse();
        Direccion               direccion       = new Direccion();
        if (addressRequest.getCIC() != null && addressRequest.getNumCorre() == null) {
            try {
                direccion = this.addressService.findAddressByCic(addressRequest.getCIC());
            } catch (Exception e) {

            }
        } else {
            /* aqui se definiran los mensajes de error siguiente SPRINT */
        }

        if (addressRequest.getCIC() != null && addressRequest.getNumCorre() != null) {
            try {
                direccion = this.addressService.findAddressByCicAndCorrelative(addressRequest.getCIC(),
                                                                               addressRequest.getNumCorre());
            } catch (Exception e) {

            }
        } else {
            /* aqui se definiran los mensajes de error siguiente SPRINT */
        }

        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }

    /**
     * =================================================================================================================
     * Descripción: Servicio de consulta para: - Lista de cliente contactabilidad
     * - Consultar los datos de Cliente Contactabilidad por CIC
     *
     * @param CIC código interno de cliente
     * @return GenericResponse<ClienteContactabilidad>
     */
    @ApiOperation(value = "Consultar los datos de Contactabilidad de un cliente. ",
                  response = ClienteContactabilidadSwg.class)
    @GetMapping(value = {"/client/contactability/{CIC}"},
                produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getClientContByCIC(
            @ApiParam(value = "Código interno de cliente") @PathVariable String CIC,
            @ApiParam(value = "Header HTTP") @RequestHeader HttpHeaders httpHeaders)
    throws Throwable
    {
        TypeSearchCustomerContactability request = new TypeSearchCustomerContactability();
        request.setCic(CIC);

        GenericResponse<?> genericResponse = customerService
                .findCustomerContactInformationByCic(httpHeaders, request);

        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }
    
    /**
     * =================================================================================================================
     * Descripción: Servicio de consulta para: - Consulta de Clientes por cic -
     * Consulta de Clientes por Tipo y Num Doc - Consultar una dirección de Cliente
     * por cic y numero correlativo
     * <p>
     * Ejemplo: requiere
     *
     * @param cic                código interno cliente
     * @param tipDoc             tipo de documento
     * @param numDoc             número de documento
     * @param typeProduct        tipo de producto
     * @param numProduct         número de producto
     * @param typeSearch         tipo de búsqueda
     * @param flagEntidadExterna flag para consultar la persona desde SUNAT o RENIEC
     * @return GenericResponse<PersonaCliente>
     */
    @ApiOperation(value = "Consulta de Clientes por: cic, typeNumDoc, typeNumProduct. ",
                  response = PersonaClienteSwg.class)
    @GetMapping(value = {"/clientSIDG/{typeSearch}"},
                produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCustomerSIDG(
            @ApiParam(value = "Tipo de búsqueda") @PathVariable String typeSearch,
            @ApiParam(value = "Tipo de documento") @RequestParam(required = false) String tipDoc,
            @ApiParam(value = "Número de documento") @RequestParam(required = false) String numDoc,
            @RequestParam(required = false) String flagEntidadExterna,
            @RequestHeader HttpHeaders httpHeaders)
    throws Throwable
    {
        typeSearch = typeSearch.toUpperCase();

        TypeSearchCustomer clientRequest = new TypeSearchCustomer(typeSearch, "", tipDoc, numDoc, "",
                                                                  "", flagEntidadExterna, "");
        GenericResponse<?> genericResponse;
        
        if (typeSearch.equals(SEARCH_NUMBER_DOCUMENT.getType())) {
        	genericResponse = customerService.findCustomerSIDGByTypeAndNumberDocument(httpHeaders, clientRequest);
        }        
        // Error
        else {
            throw new TypeSearchNotFoundException("TypeSearch: " + typeSearch);
        }        

        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }
     
/*Prueba12*/
    
    /**
     * =================================================================================================================
     * Descripción: Gestor de datos de Persona (Cliente o Usuario).
     * <p>
     * Ejemplo: requiere
     *
     * @param userApplication    Usuario que realiza la consulta (usuario de 4 letras)
     * @param cic                Cic del Cliente.
     * @param tipDoc             Tipo de Documento de busqueda
     * @param numDoc             Numero de Documento busqueda
     * @param typeProduct        Tipo de Producto busqueda
     * @param numProduct         Numero de Producto busqueda
     * @param typeSearch         tipo de búsqueda
     * @param flagEntidadExterna flag de consulta
     * @return GenericResponse<PersonaCliente>
     */
    
    @ApiOperation(value = "Consulta de datos de Persona (Cliente o Usuario). ",
            response = ClientSwg.class)
    @GetMapping(value = {"/clients/client/v1/search/{typeSearch}"},
          produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getClient(
    		@ApiParam(value = "Tipo de Busqueda") @PathVariable String typeSearch,
    		@ApiParam(value = "Usuario") @RequestParam(required = true) String userApplication,
            @ApiParam(value = "Código interno cliente") @RequestParam(required = false) String cic,
            @ApiParam(value = "Tipo de documento") @RequestParam(required = false) String typeDoc,
            @ApiParam(value = "Número de documento") @RequestParam(required = false) String numDoc,
            @ApiParam(value = "Tipo de producto") @RequestParam(required = false) String typeProduct,
            @ApiParam(value = "Número de producto") @RequestParam(required = false) String numProduct,
            @ApiParam(value = "Flag de consulta") @RequestParam(required = false) String flagEntidadExterna,
            @RequestHeader HttpHeaders httpHeaders)
    throws Throwable
    {

    	List<String> errorMessages = new ArrayList<>();

    	DataUtil dataUtil = new DataUtil();
        // Validacion de los parametros de busqueda
        dataUtil.validateParameter(userApplication, "userApplication", 4, 6, false, errorMessages);
		dataUtil.validateParameter(cic, "cic", 2, 12, false, errorMessages);
        dataUtil.validateParameter(typeDoc, "typeDoc", 1, 1, false, errorMessages);
        dataUtil.validateParameter(numDoc, "numDoc", 2, 12, true, errorMessages);
        dataUtil.validateParameter(typeProduct, "typeProduct", 1, 2, false, errorMessages);
        dataUtil.validateParameter(numProduct, "numProduct", 3, 18, true, errorMessages);
        dataUtil.validateParameter(flagEntidadExterna, "flagEntidadExterna", 1, 1, false, errorMessages);
        
        // Si hay errores, devolver un HttpStatus.BAD_REQUEST con el mensaje de error
        if (!errorMessages.isEmpty()) {
            String errorMessage = String.join("\n", errorMessages);
            return ResponseEntity.badRequest().body(errorMessage);
        }
        
        //Objeto que encapsula los criterios de búsqueda del cliente 
        TypeSearchCustomer clientRequest = new TypeSearchCustomer(typeSearch.toUpperCase(),
                cic, typeDoc, numDoc, typeProduct, numProduct, flagEntidadExterna, userApplication
        );

        GenericResponse<CustomResponse> genericResponse;
        
        // Consulta por el CIC
        if (typeSearch.equals(SEARCH_CIC.getType())) {
        	genericResponse = customerService.findCustomerByCic(httpHeaders, clientRequest);
        }
        // Consulta por el numero de documento
        else if (typeSearch.equals(SEARCH_NUMBER_DOCUMENT.getType())) {
        	genericResponse = customerService.findCustomerByTypeAndNumberDocument(httpHeaders, clientRequest);
        }
        // Consulta por el numero del producto
        else if (typeSearch.equals(SEARCH_NUMBER_PRODUCT.getType())) {
        	genericResponse = customerService.findCustomerByTypeAndNumberProduct(httpHeaders, clientRequest);
        }
        // Exception de error
        else {
            throw new TypeSearchNotFoundException("TypeSearch: " + typeSearch);
        }  

        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }

}
