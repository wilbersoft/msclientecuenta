package pe.com.bn.ms.api;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.dto.Direccion;
import pe.com.bn.ms.dto.PersonaCliente;
import pe.com.bn.ms.service.customer.CustomerUpdateService;

/**
 * Descripción: Api solo para consultas (U -> UPDATE)
 */
@RestController
@RequestMapping("/msdataclients")
@Api(tags = "Apis de actualización - DataClients",
     description = "Actualización de clientes y derivados. ")
public class CustomerUpdateApi
{
    private final CustomerUpdateService customerService;

    @Autowired
    public CustomerUpdateApi(CustomerUpdateService customerService)
    {this.customerService = customerService;}

    /**
     * =================================================================================================================
     * Descripción: Servicio de consulta para:
     * - Actualizar un persona (PN o PJ)
     *
     * @param CIC            código interno de cliente
     * @param personaCliente objeto persona
     * @return OK
     * @throws Exception error
     */
    @ApiOperation(value = "Actualizar los datos de un Cliente. ", response = String.class)
    @PatchMapping(value = {"/client/{CIC}"},
                  consumes = MediaType.APPLICATION_JSON_VALUE,
                  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateClient(@ApiParam(value = "Código interno de cliente") @PathVariable String CIC,
                                          @ApiParam(name = "PersonaCliente", value = "Json PersonaCliente")
                                          @RequestBody PersonaCliente personaCliente,
                                          @RequestHeader HttpHeaders httpHeaders)
    throws Throwable
    {
        personaCliente.setNumDocument(CIC);

        GenericResponse<?> genericResponse = customerService
                .updateClientByCIC(personaCliente, httpHeaders);

        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }

    /**
     * =================================================================================================================
     * Descripción: Servicio de consulta para:
     * - Actualizar una dirección de Cliente
     *
     * @param CIC       código interno de cliente
     * @param numCorre  número correlativo
     * @param direccion objeto dirección
     * @return OK
     */
    @ApiOperation(value = "Actualizar los datos de una Dirección.", response = String.class)
    @PatchMapping(value = {"/address/{CIC}/{numCorre}"},
                  consumes = MediaType.APPLICATION_JSON_VALUE,
                  produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateClientAddress(
            @ApiParam(value = "Código interno de cliente") @PathVariable String CIC,
            @ApiParam(value = "Número correlativo") @PathVariable String numCorre,
            @ApiParam(name = "Direccion", value = "Json Direccion")
            @RequestBody Direccion direccion,
            @RequestHeader HttpHeaders httpHeaders
    ) throws Exception
    {
        return new ResponseEntity<>("OK", HttpStatus.OK);
    }

}
