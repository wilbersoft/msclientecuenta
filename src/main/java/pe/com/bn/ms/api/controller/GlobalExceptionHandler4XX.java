package pe.com.bn.ms.api.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.dto.ErrorResponse;
import pe.com.bn.ms.exception.base4XX.FeignHttpException;
import pe.com.bn.ms.exception.base4XX.FieldConstraintException;
import pe.com.bn.ms.exception.base4XX.GatewayException;
import pe.com.bn.ms.exception.base4XX.HostResponseException;

import static org.springframework.http.HttpStatus.OK;

public class GlobalExceptionHandler4XX extends ResponseEntityExceptionHandler
{
    /**
     * =================================================================================================================
     * Excepción genérica
     *
     * @param ex      excepción personalizada
     * @param request interfaz genérica web
     * @return {@link ResponseEntity}
     */
    @ExceptionHandler(GatewayException.class)
    public final ResponseEntity<?> handleGatewayException(GatewayException ex, WebRequest request)
    {
        GenericResponse<?> genericResponse = new GenericResponse<>(
        		 ex.getErrorCode(),
                 new ErrorResponse(ex.getClass().toString(),
                                   ex.getError().getHttpStatus(),
                                   request.getDescription(false),
                                   ex.getLocalizedMessage(),
                                   ex.getParameters(),
                                   ex.getError().getMsgError()),
                 ex.getFriendlyMessage(),
                 ex.getTechnicalMessage()
        );

        return new ResponseEntity<>(genericResponse, OK);
    }

    /**
     * =================================================================================================================
     * Excepción controlada de consulta de errores de clientes http
     *
     * @param ex      excepción personalizada
     * @param request interfaz genérica web
     * @return {@link ResponseEntity}
     */
    @ExceptionHandler(FeignHttpException.class)
    public final ResponseEntity<?> handleFeignHttpException(FeignHttpException ex, WebRequest request)
    {
        GenericResponse<?> genericResponse = new GenericResponse<>(
                ex.getErrorCode(),
                new ErrorResponse(ex.getClass().toString(),
                                  ex.getError().getHttpStatus(),
                                  request.getDescription(false),
                                  ex.getLocalizedMessage(),
                                  ex.getParameters(),
                                  ex.getError().getMsgError()),
                ex.getFriendlyMessage(),
                ex.getTechnicalMessage()
        );

        return new ResponseEntity<>(genericResponse, OK);
    }

    /**
     * =================================================================================================================
     * Excepción controlada de consulta de errores de clientes http
     *
     * @param ex      excepción personalizada
     * @param request interfaz genérica web
     * @return {@link ResponseEntity}
     */
    @ExceptionHandler(FieldConstraintException.class)
    public final ResponseEntity<?> handleFieldConstraintException(FieldConstraintException ex, WebRequest request)
    {
        GenericResponse<?> genericResponse = new GenericResponse<>(
                ex.getErrorCode(),
                new ErrorResponse(ex.getClass().toString(),
                                  null,
                                  request.getDescription(false),
                                  ex.getLocalizedMessage(),
                                  ex.getParameters(),
                                  ""),
                ex.getFriendlyMessage(),
                ex.getTechnicalMessage()
        );

        return new ResponseEntity<>(genericResponse, OK);
    }

    /**
     * =================================================================================================================
     * Excepción controlada de consulta de errores de clientes http
     *
     * @param ex      excepción personalizada
     * @param request interfaz genérica web
     * @return {@link ResponseEntity}
     */
    @ExceptionHandler(HostResponseException.class)
    public final ResponseEntity<?> handleFieldConstraintException(HostResponseException ex, WebRequest request)
    {
        GenericResponse<?> genericResponse = new GenericResponse<>(
                ex.getErrorCode(),
                new ErrorResponse(ex.getClass().toString(),
                                  null,
                                  request.getDescription(false),
                                  ex.getLocalizedMessage(),
                                  ex.getParameters(),
                                  ""),
                ex.getFriendlyMessage(),
                ex.getTechnicalMessage()
        );

        return new ResponseEntity<>(genericResponse, OK);
    }

}
