package pe.com.bn.ms.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.domain.TypeSearchProduct;
import pe.com.bn.ms.domain.response.DataProduct.CustomProductResponse;
import pe.com.bn.ms.dto.Product;
import pe.com.bn.ms.exception.base4XX.TypeSearchNotFoundException;
import pe.com.bn.ms.model.ProductSwg;
import pe.com.bn.ms.service.product.ProductService;
import pe.com.bn.ms.util.Shared.DataUtil;

import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_CIC;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_NUMBER_DOCUMENT;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_NUMBER_PRODUCT;

import java.util.ArrayList;
import java.util.List;

/**
 * @author prov_destrada
 * @created 22/10/2020 - 06:20 p. m.
 * @project ms-dataclients
 */
@RestController
@RequestMapping("/msdataclients/product")
@Api(tags = "Apis de consulta - DataProducts",
     value = "Consulta de los productos de clientes. ")
public class ProductReadApi
{
    private final ProductService productService;

    @Autowired
    public ProductReadApi(ProductService productService) {this.productService = productService;}

//    /**
//     * =================================================================================================================
//     * Consultar producto {@link Product}
//     *
//     * @param httpHeaders cabeceras de la aplicación
//     * @param cic         código único de cliente
//     * @return {@link java.util.List<Product>}
//     * @throws Throwable error
//     */
//    @GetMapping(value = "/search/{type_search}",
//                produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<?> getProduct(@PathVariable("type_search") String typeSearch,
//                                        @RequestHeader HttpHeaders httpHeaders,
//                                        @RequestParam(required = false) String cic,
//                                        @RequestParam(required = false) String tipDoc,
//                                        @RequestParam(required = false) String numDoc) throws Throwable
//    {
//    	//INICIO MGL
//        TypeSearchProduct searchProduct = new TypeSearchProduct();
//        searchProduct.setCic(cic);
//        searchProduct.setTypeDocument(tipDoc);
//        searchProduct.setNumberDocument(numDoc);
//
//      //FIN MGL
//        GenericResponse<?> genericResponse;
//
//        
//        typeSearch = typeSearch.toUpperCase();
//        if (typeSearch.equals(SEARCH_NUMBER_DOCUMENT.getType())) {
//            genericResponse = productService
//                    .findProductByTypeAndNumberDocument(httpHeaders, searchProduct);
//        }
//        //CIC
//        else if (typeSearch.equals(SEARCH_CIC.getType())) {
//            genericResponse = productService
//                    .findProductByCic(httpHeaders, searchProduct);
//        }
//        // Tipo de búsqueda no existe
//        else {
//            throw new TypeSearchNotFoundException("TypeSearch: " + typeSearch);
//        }
//
//        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
//    }
//    

    /**
     * =================================================================================================================
     * Consultar producto {@link Product}
     *
     * @param httpHeaders cabeceras de la aplicación
     * @param userApplication         usuario
     * @param cic         código único de cliente
     * @param typeDoc         tipo de documento
     * @param numDoc         numero de documento
     * @param typeProduct         tipo de producto
     * @param numProduct         numero del producto
     * @return {@link java.util.List<Product>}
     * @throws Throwable error
     */
    @ApiOperation(value = "Consulta de datos de Producto",
            response = ProductSwg.class)
    @GetMapping(value = "/clients/product/v1/search/{typeSearch}",
                produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getCustomerProduct(
    		@ApiParam(value = "Tipo de Busqueda") @PathVariable String typeSearch,
    		@ApiParam(value = "Usuario") @RequestParam(required = true) String userApplication,
    	    @ApiParam(value = "Codigo interno del cliente") @RequestParam(required = false) String cic,
    		@ApiParam(value = "Tipo de documento") @RequestParam(required = false) String typeDoc,
    		@ApiParam(value = "Número de documento") @RequestParam(required = false) String numDoc,
    		@ApiParam(value = "Tipo de producto") @RequestParam(required = false) String typeProduct,
    		@ApiParam(value = "Número de producto") @RequestParam(required = false) String numProduct,
    		@RequestHeader HttpHeaders httpHeaders)
            throws Throwable
    {
    	List<String> errorMessages = new ArrayList<>();
    	
    	DataUtil dataUtil = new DataUtil();
    	// Validacion de los parametros de busqueda
        dataUtil.validateParameter(userApplication, "userApplication", 4, 6, false, errorMessages);
        dataUtil.validateParameter(cic, "cic", 2, 12, false, errorMessages);
        dataUtil.validateParameter(typeDoc, "typeDoc", 1, 1, false, errorMessages);
        dataUtil.validateParameter(numDoc, "numDoc", 2, 12, true, errorMessages);
        dataUtil.validateParameter(typeProduct, "typeProduct", 1, 2, false, errorMessages);
        dataUtil.validateParameter(numProduct, "numProduct", 3, 18, true, errorMessages);
        
        // Si hay errores, devolver un HttpStatus.BAD_REQUEST con el mensaje de error
        if (!errorMessages.isEmpty()) {
            String errorMessage = String.join("\n", errorMessages);
            return ResponseEntity.badRequest().body(errorMessage);
        }
        
        //Objeto que encapsula los criterios de búsqueda del cliente 
        TypeSearchProduct searchProduct = new TypeSearchProduct(typeSearch.toUpperCase(),
                cic, typeDoc, numDoc, typeProduct, numProduct, userApplication
        );
        
        GenericResponse<CustomProductResponse> genericResponse;

        // Consulta por el CIC
        if (typeSearch.equals(SEARCH_CIC.getType())) {
        	genericResponse = productService.findProductByCic(httpHeaders, searchProduct);
        }
        // Consulta por el numero de documento
        else if (typeSearch.equals(SEARCH_NUMBER_DOCUMENT.getType())) {
        	genericResponse = productService.findProductByTypeAndNumberDocument(httpHeaders, searchProduct);
        }
        // Consulta por el numero del producto
        else if (typeSearch.equals(SEARCH_NUMBER_PRODUCT.getType())) {
        	genericResponse = productService.findCustomerByTypeAndNumberProduct(httpHeaders, searchProduct);
        }
        // Exception de error
        else {
            throw new TypeSearchNotFoundException("TypeSearch: " + typeSearch);
        }  

        return new ResponseEntity<>(genericResponse, HttpStatus.OK);
    }
    
}
