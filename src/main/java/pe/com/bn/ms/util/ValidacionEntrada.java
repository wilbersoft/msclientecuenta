package pe.com.bn.ms.util;

public class ValidacionEntrada {


    public   String completarCerosIzquierda(String texto, int longitudDeseada) {
        int longitudTexto = texto.length();

        if (longitudTexto >= longitudDeseada) {
            return texto;
        } else {
            int cantidadCeros = longitudDeseada - longitudTexto;
            StringBuilder resultado = new StringBuilder();

            for (int i = 0; i < cantidadCeros; i++) {
                resultado.append("0");
            }

            resultado.append(texto);
            return resultado.toString();
        }
    }
    public  boolean validarLongitudNumeroDocumento(String texto) {
        int longitud = texto.length();
        return longitud >= 3 && longitud <= 12;
    }
    public  boolean validaTipoDocumentoEnLista(char[] caracteres) {

        //char[] caracteres = {'A', 'B', '1', '3', '4', '5', '6'};

        for (char c : caracteres) {
            if (esValido(c)) {
                System.out.println(c + " es válido.");
                return true;
            } else {
                System.out.println(c + " no es válido.");
                return false;
            }
        }
        return false;
    }

    public  boolean esValido(char c) {
        return c == 'A' || c == 'B' || c == '1' || c == '3' || c == '4' || c == '5' || c == '6';
    }


    public   String completarCerosDerecha(String cadena, int longitud) {
        StringBuilder sb = new StringBuilder(cadena);

        while (sb.length() < longitud) {
            sb.append("0");
        }

        return sb.toString();
    }

}
