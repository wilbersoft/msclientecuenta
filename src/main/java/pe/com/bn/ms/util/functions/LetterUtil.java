package pe.com.bn.ms.util.functions;

import lombok.extern.log4j.Log4j2;

/**
 * @author prov_destrada
 * @created 29/10/2020 - 05:16 p. m.
 * @project ms-dataclients
 */
@Log4j2
public class LetterUtil
{
    /**
     * =================================================================================================================
     * Capitalizar la primera letra de una
     *
     * @param word palabra a capitalizar
     * @return letter
     */
    public static String capitalizeFirstLetter(String word) throws Throwable
    {
        if (word == null || word.length() == 0) {
            return word;
        }
        // En caso ya esté capitalizado
        else if (Character.isUpperCase(word.charAt(1))) {
            return word;
        }

        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }
}
