package pe.com.bn.ms.util.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Parameters
{
    NUEVA_ALTA_CLIENTE("A"),
    NUEVA_ALTA_DIRECCION("A"),
    NUEVA_ALTA_CLIENTE_TIPO("CL"),
    NUEVA_ALTA_DIRECCION_TIPO("DH"),
    NUEVA_ALTA_CLIENTE_USUARIO("UMSC"),

    MODIFICACION_CLIENTE("M"),
    MODIFICACION_DIRECCION("M"),
    MODIFICACION_CLIENTE_TIPO("CL"),
    MODIFICACION_DIRECCION_TIPO("DH");

    private String code;
}
