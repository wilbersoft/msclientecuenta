package pe.com.bn.ms.util.functions;

import lombok.extern.log4j.Log4j2;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author prov_destrada
 * @created 23/10/2020 - 01:08 p. m.
 * @project ms-dataclients
 */
@Log4j2
public class DateTimePicker
{
    /**
     * =================================================================================================================
     * Generar fecha actual con formato de envío a trama gateway '2020-02-17-09-50-11-999999'
     *
     * @return tiempo
     */
    public static String gatewayTimestamp()
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss-999999");
        return LocalDateTime.now().format(formatter);
    }
}
