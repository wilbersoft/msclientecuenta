package pe.com.bn.ms.util.functions;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import pe.com.bn.ms.domain.Headers;
import pe.com.bn.ms.tracert.domain.RabbitObjectData;
import pe.com.bn.ms.tracert.domain.RabbitPlot;

import java.lang.annotation.Annotation;
import java.util.LinkedHashMap;
import java.util.Map;

import static pe.com.bn.ms.util.functions.ReflectionsUtil.httpHeadersToHeaders;
import static pe.com.bn.ms.util.functions.ReflectionsUtil.setterNoSorted;

/**
 * @author prov_destrada
 * @created 29/10/2020 - 01:00 p. m.
 * @project ms-dataclients
 */
@Log4j2
public class TraceUtil
{
    private static final ObjectMapper mapper = initObjectMapper();

    private static ObjectMapper initObjectMapper()
    {
        return new ObjectMapper();
    }

    /**
     * =================================================================================================================
     * Extraer los parámetros de los argumentos que pasan por el método
     *
     * @param joinPoint contiene todos los argumentos que pasan por el método
     * @return {@link HttpHeaders}
     */
    public static RabbitPlot extractInputBodyRequest(JoinPoint joinPoint)
    {
        Object[] elements = joinPoint.getArgs();

        RabbitPlot rabbitPlot = new RabbitPlot();

        // --------------------------------------------
        // Recorrer cada uno de los argumentos
        for (Object element : elements) {
            // Cabeceras HttpHeaders
            if (element instanceof HttpHeaders) {
                HttpHeaders httpHeaders = (HttpHeaders) element;

                try {
                    Headers headers = httpHeadersToHeaders(httpHeaders);
                    rabbitPlot = setterNoSorted(rabbitPlot, headers);

                } catch (Throwable ex) {
                    log.error("Ocurrió un error al convertir los headers: {}", ex.getMessage());
                }

            }

            // INPUT's
            else {
                // --------------------------------------------
                // Extraer de la anotación RequestBody en input
                MethodSignature methodSignature  = (MethodSignature) joinPoint.getSignature();
                Annotation[][]  annotationMatrix = methodSignature.getMethod().getParameterAnnotations();

                int index = -1;
                for (Annotation[] annotations : annotationMatrix) {
                    index++;
                    for (Annotation annotation : annotations) {
                        if (!(annotation instanceof RequestBody))
                            continue;
                        RabbitObjectData rabbitObjectData = new RabbitObjectData();
                        rabbitObjectData.setInput(elements[index]);

                        LinkedHashMap<String, Object> lhmInput = mapper
                                .convertValue(elements[index], LinkedHashMap.class);

                        Object typeOp = lhmInput.entrySet()
                                .stream()
                                .filter(f -> f.getKey().contains("typeOp"))
                                .map(Map.Entry::getValue)
                                .findFirst()
                                .orElse("ADMIN");

                        rabbitPlot.setTypeOp(typeOp.toString());
                        rabbitPlot.setData(rabbitObjectData);
                    }
                }
            }
        }

        return rabbitPlot;
    }
}
