package pe.com.bn.ms.util.Shared;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class DataUtil {

	/**
     * =================================================================================================================
     * Validar los parametros de busqueda del cliente
     */
	public void validateParameter(String value, String paramName, int minLength, int maxLength, boolean isAlphaNumeric, List<String> errorMessages) {
    	
    	if (value == null) {
            return;
        }
    	
    	if (value.length() < minLength || value.length() > maxLength) {
            errorMessages.add(String.format("Error: %s debe tener un valor entre %d y %d caracteres.", paramName, minLength, maxLength));
        }

        if (isAlphaNumeric) {
            String alphaNumericRegex = "^[a-zA-Z0-9]+$";
            if (!value.matches(alphaNumericRegex)) {
                errorMessages.add(String.format("Error: %s debe ser alfanumérico.", paramName));
            }
        }
    }

	/**
     * =================================================================================================================
     * Formateo de Trama 
     */
	public static String formatearCampo(Object valor, int tamano, String tipo, boolean obligatorio, int posicionInicial, int posicionFinal) {
	    // Convierte el valor a una cadena, o una cadena vacía si es nulo
	    String valorStr = (valor != null) ? valor.toString() : "";

	    switch (tipo) {
        case "N":
        	// Si el tipo es "N" (número), formatea el valorStr según las reglas establecidas
	        // Si es vacío y obligatorio, rellena con espacios en blanco, de lo contrario, deja la cadena vacía
	        // Si contiene un signo "-", rellena con espacios a la derecha, de lo contrario, rellena con ceros a la izquierda
	        valorStr = valorStr.isEmpty() ? (obligatorio ? StringUtils.leftPad("", tamano) : StringUtils.rightPad("", tamano)) : (valorStr.contains("-") ? StringUtils.rightPad(valorStr, tamano) : StringUtils.leftPad(valorStr, tamano, '0'));
            break;
        case "C":
        	// Si el tipo es "C" (cadena), rellena el valorStr con espacios en blanco si es vacío y obligatorio
	        if (valorStr.isEmpty() && obligatorio) {
	            throw new IllegalArgumentException("El campo es obligatorio");
	        }
	        valorStr = StringUtils.rightPad(valorStr, tamano);
            break;
        default:
            // Si el tipo no es válido, lanza una excepción
            throw new IllegalArgumentException("Tipo de campo no válido");
	    }

	    // Se obtiene la subcadena desde la posición inicial hasta la posición final indicada
	    if (valorStr.length() > posicionInicial && valorStr.length() >= posicionFinal) {
	        valorStr = valorStr.substring(posicionInicial, posicionFinal);
	    }

	    // Devuelve el valorStr formateado
	    return valorStr;
	}
	
}
