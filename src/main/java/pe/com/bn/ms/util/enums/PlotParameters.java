package pe.com.bn.ms.util.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author prov_destrada
 * @created 14/10/2020 - 05:18 p. m.
 * @project MSDataClients
 */
@Getter
@AllArgsConstructor
@SuppressWarnings("all")
public enum PlotParameters
{
    // Headers
    HEADER("HEADER", "", ""),
    BODY("BODY", "", ""),
    HEADER_TRANSACTION_PC01("HEADER", "Programa host", "PC01"),
    HEADER_CODE_TRANSACTION_612("HEADER", "", "612"),
    HEADER_CODE_TRANSACTION_WS00("HEADER", "", "WS00"),
    HEADER_LENGTH("HEADER", "", "9999"),
    HEADER_LENGTH_SIUC("HEADER", "", "SIUC"),
    HEADER_MSG_NO("HEADER", "", "0"),
    HEADER_COD_USER("HEADER", "", "DJRS"),
    HEADER_USER("HEADER", "User", "ARRV"),

    // Operaciones
    OPERATION_CREATE("", "", "CREATE"),
    OPERATION_READ("", "", "CONSULTA"),
    OPERATION_UPDATE("", "", "UPDATE"),
    OPERATION_DELETE("", "", ""),

    // Colofón
    COLOFON_CREATE("", "", ""),
    COLOFON_READ("", "", ""),
    COLOFON_UPDATE("", "", ""),
    COLOFON_DELETE("", "", ""),

    // Tipos de campo
    TYPE_CHARACTER("CN", "Tipo caracter, padding derecho ' ' ", ""),
    TYPE_CHARACTER_0("CN0", "Tipo caracter, padding izquierdo '0' ", ""),
    TYPE_NUMBER("N", "Tipo numérico, padding izquierdo '0' ", ""),

    // Checkpoints
    CHECKPOINT_NESTED("N", "Check para objetos anidados", ""),
    CHECKPOINT_RESPONSE("R", "Check para respuestas de HOST después de una transacción", ""),
    CHECKPOINT_LIST("*", "Sirve para extraer el tamaño de una lista", ""),

    // Check's
    CHECK("X", "Check general", ""),
    CHECK_NESTED("N", "Check para objetos anidados", ""),

    // Tipo de objeto
    OBJECT_NESTED("NESTED", "", ""),
    OBJECT_LIST("LIST", "", ""),

    // ---------------------------------------------------
    ;

    private final String type;
    private final String name;
    private final String value;
}
