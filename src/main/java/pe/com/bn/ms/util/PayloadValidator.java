package pe.com.bn.ms.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.eclipse.jetty.client.HttpResponseException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.domain.Headers;
import pe.com.bn.ms.dto.GatewayResponse;
import pe.com.bn.ms.exception.EndpointException;
import pe.com.bn.ms.exception.base4XX.ResourceNotFoundException;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static pe.com.bn.ms.util.enums.Messages.SUCCESSFUL;

@Log4j2
@SuppressWarnings("all")
public class PayloadValidator
{
    private static final ObjectMapper mapper = initObjectMapper();

    private static ObjectMapper initObjectMapper()
    {
        return new ObjectMapper();
    }

    /**
     * =================================================================================================================
     * Validar la respuesta de una api, permite ingresar parámetros de control
     *
     * @param response ResponseEntity<?>
     * @return GenericResponse
     * @throws Exception error
     */
    public static GenericResponse<?> apiConverter(ResponseEntity<?> response, String params) throws Throwable
    {
        if (response.getStatusCode().equals(HttpStatus.OK))
            if (response.getBody() != null) {

                GenericResponse<?> genericResponse = mapper
                        .convertValue(response.getBody(), GenericResponse.class);

                if (genericResponse.getCodResult().matches("00000|0000"))
                    return genericResponse;
                else
                    // En caso el cliente responda con un código de error.
                    throw new EndpointException(genericResponse);
            } else
                // En caso el body este vacio o nulo.
                throw new ResourceNotFoundException(params);
        else
            // En caso la respuesta http, sea diferente a lo esperado OK.
            throw new HttpResponseException(response.toString(), null);
    }

    /**
     * =================================================================================================================
     * Validar la respuesta de una api
     *
     * @param response ResponseEntity<?>
     * @return GenericResponse
     * @throws Exception error
     */
    public static GenericResponse<?> apiConverterNoParam(ResponseEntity<?> response) throws Throwable
    {
        if (response.getStatusCode().equals(HttpStatus.OK)) {
            if (response.getBody() != null) {

                GenericResponse<?> genericResponse = mapper
                        .convertValue(response.getBody(), GenericResponse.class);

                if (genericResponse.getCodResult().matches("00000|0000"))
                    return genericResponse;
                else
                    // En caso el cliente responda con un código de error.
                    throw new EndpointException(genericResponse);
            } else {
                throw new Throwable("El cliente no ha retornado ningún valor");
            }
        } else {
            throw new HttpResponseException(response.toString(), null);
        }


    }

    /**
     * =================================================================================================================
     * Convertir un {@link HttpHeaders} a {@link Headers}
     *
     * @param httpHeaders {@link HeaderDTO}
     * @return {@link HttpHeaders}
     */
    public static HttpHeaders httpHeadersForGateway(HttpHeaders httpHeaders) throws Throwable
    {
        HttpHeaders gatewayHeader = new HttpHeaders();
        gatewayHeader.add("canal", httpHeaders.getFirst("Channel"));
        gatewayHeader.add("tipoOperacion", "ADMIN");
        gatewayHeader.add("aplicacion", httpHeaders.getFirst("Application"));

        return gatewayHeader;
    }

    /**
     * =================================================================================================================
     * Validar respuesta del gateway
     *
     * @param responseEntity {@link ResponseEntity<GatewayResponse>}
     * @return {@link GatewayResponse}
     * @throws Throwable error
     */
    public static GatewayResponse validateGatewayResponse(ResponseEntity<GatewayResponse> responseEntity)
    throws Throwable
    {
        GatewayResponse gatewayResponse;

        // Validar la respuesta
        if (responseEntity.getBody() != null) {

            if (responseEntity.getBody().getMsgno().equals(SUCCESSFUL.getCodResult())) {

                gatewayResponse = responseEntity.getBody();
            } else {
                gatewayResponse = responseEntity.getBody();
            }
        } else {
            throw new ResourceNotFoundException();
        }

        return gatewayResponse;
    }

    /**
     * =================================================================================================================
     * Extraer los campos del error
     *
     * @param bindingResult {@link BindingResult}
     * @return lista de campos de error concatenados
     */
    public static String extractThrowFields(BindingResult bindingResult)
    {
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        return fieldErrors.toString();
    }

    /**
     * =================================================================================================================
     * Generar fecha con formato dd/MM/yyyy HH:mm:ss
     *
     * @return fecha con formato
     */
    public static String nowTime()
    {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
        return LocalDateTime.now().format(formatter);
    }

    /**
     * =================================================================================================================
     * Convertir un objeto dto a json string
     *
     * @param value dto
     * @return
     */
    public static String writeValueAsString(Object value)
    {
        String valueAsString = "";

        try {
            valueAsString = mapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            log.error("Se ha producido un error al serializar el objeto: {}", e.getMessage());
        }

        return valueAsString;
    }

    /**
     * =================================================================================================================
     * Verifica si un objeto es nulo o vacío
     *
     * @param object       objeto a comparar
     * @param defaultValue valor a devolver
     * @param <T>          object
     * @return defaultValue
     */
    public static <T> T defaultIfNull(final T object, final T defaultValue)
    {
        return object != null ? object : defaultValue;
    }

    /**
     * =================================================================================================================
     * Verifica si un objeto es nulo o vacío
     *
     * @param object       objeto a comparar
     * @param defaultValue valor a devolver
     * @param <T>          object
     * @return defaultValue
     */
    public static <T> T defaultIfBlank(final T object, final T defaultValue)
    {
        return object != null && !object.equals("") ? object : defaultValue;
    }

    /**
     * =================================================================================================================
     *
     * @param now
     * @return
     * @throws Exception
     */
    public static long convertToMilis(String now) throws Exception
    {
        LocalDateTime ldtNow    = LocalDateTime.parse(now);
        Timestamp     timestamp = Timestamp.valueOf(ldtNow);
        return timestamp.getTime();
    }

    /**
     * =================================================================================================================
     *
     * @param now
     * @return
     * @throws Exception
     */
    public static long convertToMilisForToTp(String now) throws Exception
    {
        LocalDateTime ldtNow    = LocalDateTime.parse(now).withHour(12).withMinute(12).withSecond(12).withNano(12);
        Timestamp     timestamp = Timestamp.valueOf(ldtNow);
        return timestamp.getTime();
    }

}
