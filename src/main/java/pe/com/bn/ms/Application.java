package pe.com.bn.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import pe.com.bn.ms.util.Shared.DataUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
@EnableAspectJAutoProxy
@EnableFeignClients("pe.com.bn.ms.payload")
@SpringBootApplication
public class Application
{

    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
        Application trama = new Application();
        System.out.println("Trama Entrada: "+ trama.obtenerDatosEntrada());
        System.out.println("Trama Salida: "+ trama.obtenerDatosSalida());
    }


    public String obtenerDatosEntrada() {
    	// Formatear los campos utilizando el método formatearCampo
    	String hdLong = DataUtil.formatearCampo("9999", 4, "C", true,1,5);
        String hdTran = DataUtil.formatearCampo("PC01", 4, "C", true,5,9);
        String hdTimestamp = DataUtil.formatearCampo("2023-04-20-00-00-00-999999", 26, "C", false,9,35);
        String hdCodTran1Format = DataUtil.formatearCampo(612, 4, "N", true,35,39);
        String hdCodUser = DataUtil.formatearCampo("DJRS", 6, "C", true,39,45);
        String hdDesRestFormat = DataUtil.formatearCampo(0, 5, "N", false,45, 50);
        String hdCodCanal = DataUtil.formatearCampo("", 4, "C", false,50,79);
        String hdCodTerm = DataUtil.formatearCampo("", 6, "C", false,79,85);
        String hdCodRetLog = DataUtil.formatearCampo("", 5, "C", false,85,90);
        String hdDesRetLog = DataUtil.formatearCampo("", 22, "C", false,90,112);
        String inTdcmtoId = DataUtil.formatearCampo("", 1, "C", false,112,113);
        String inNdcmtoIdFormat = DataUtil.formatearCampo(0, 12, "N", true,113,125);
        String inTipoBusFormat = DataUtil.formatearCampo(2, 2, "N", true,125,127);
        String inTipoResultFormat = DataUtil.formatearCampo(1, 2, "N", true,127,129);
        String inUltctaFormat = DataUtil.formatearCampo(0, 4, "N", false,129,133);
        String inCicFormat = DataUtil.formatearCampo(5243466, 12, "N", true,133,145);
        String inCodProdFormat = DataUtil.formatearCampo(0, 2, "N", false,145,147);
        String inNumProdFormat = DataUtil.formatearCampo(0, 18, "N", false,147,165);
        String inNumCorDirFormat = DataUtil.formatearCampo(0, 5, "N", false,165,170);
        String inNtarjetaInputFormat = DataUtil.formatearCampo(0, 16, "N", false,170,186);
        String inTdocumAdFormat = DataUtil.formatearCampo(0, 2, "N", false,186,188);
        String inFiller = DataUtil.formatearCampo("", 24, "C", false,188,212);
        String HD_DES_RET = DataUtil.formatearCampo(0, 25, "N", false,50,75);
        

        // Construir la cadena con los campos formateados
        StringBuilder tramaFormateada = new StringBuilder();
        tramaFormateada.append(hdLong).append(hdTran).append(hdTimestamp).append(hdCodTran1Format).
        append(hdCodUser).append(hdDesRestFormat).append(HD_DES_RET).append(hdCodCanal).append(hdCodTerm).append(hdCodRetLog).
        append(hdDesRetLog).append(inTdcmtoId).append(inNdcmtoIdFormat).append(inTipoBusFormat).append(inTipoResultFormat).
        append(inUltctaFormat).append(inCicFormat).append(inCodProdFormat).append(inNumProdFormat).append(inNumCorDirFormat).
        append(inNtarjetaInputFormat).append(inTdocumAdFormat).append(inFiller);
    	
    	return tramaFormateada.toString();
    }
    
    public String obtenerDatosSalida() {
    	
    	String hdLong = DataUtil.formatearCampo("9999", 4, "C", true,1,5);
        String hdTran = DataUtil.formatearCampo("PC01", 4, "C", true,5,9);
        String hdTimestamp = DataUtil.formatearCampo("2023-04-20-00-00-00-999999", 26, "C", false,9,35);
        String hdCodTran1Format = DataUtil.formatearCampo(612, 4, "N", true,35,39);
        String hdCodUser = DataUtil.formatearCampo("DJRS", 6, "C", true,39,45);
        String hdDesRestFormat = DataUtil.formatearCampo(0, 5, "N", false,45, 50);
        String hdCodCanal = DataUtil.formatearCampo("", 4, "C", false,50,79);
        String hdCodTerm = DataUtil.formatearCampo("", 6, "C", false,79,85);
        String hdCodRetLog = DataUtil.formatearCampo("", 5, "C", false,85,90);
        String hdDesRetLog = DataUtil.formatearCampo("", 22, "C", false,90,112);
        String inTdcmtoId = DataUtil.formatearCampo("", 1, "C", false,112,113);
        String inNdcmtoIdFormat = DataUtil.formatearCampo(0, 12, "N", true,113,125);
        String inTipoBusFormat = DataUtil.formatearCampo(2, 2, "N", true,125,127);
        String inTipoResultFormat = DataUtil.formatearCampo(1, 2, "N", true,127,129);
        String inUltctaFormat = DataUtil.formatearCampo(0, 4, "N", false,129,133);
        String inCicFormat = DataUtil.formatearCampo(5243466, 12, "N", true,133,145);
        String inCodProdFormat = DataUtil.formatearCampo(0, 2, "N", false,145,147);
        String inNumProdFormat = DataUtil.formatearCampo(0, 18, "N", false,147,165);
        String inNumCorDirFormat = DataUtil.formatearCampo(0, 5, "N", false,165,170);
        String inNtarjetaInputFormat = DataUtil.formatearCampo(0, 16, "N", false,170,186);
        String inTdocumAdFormat = DataUtil.formatearCampo(0, 2, "N", false,186,188);
        String inFiller = DataUtil.formatearCampo("", 24, "C", false,188,212);
        String HD_DES_RET = DataUtil.formatearCampo("OPERACION OK", 25, "C", false,50,75);
        
        String ClteCinternoCic = DataUtil.formatearCampo("000005243466", 12, "C", true,212,224);   
        String ClteCoficIngreso = DataUtil.formatearCampo("0026", 4, "C", true,224,228);  
        String ClteCoficOperac = DataUtil.formatearCampo("0000", 4, "C", true,228,232);   
        String ClteFingresoBco = DataUtil.formatearCampo("20081110", 8, "C", true,232,240);
        String ClteCejectNeg = DataUtil.formatearCampo("", 6, "N", true,240,246);  
        String des = DataUtil.formatearCampo("", 25, "C", false,246,271);        
        String ClteCsectorEcon = DataUtil.formatearCampo("001420", 6, "C", true,271,277);       
        String ClteCsectorEconDes = DataUtil.formatearCampo("SECT.PRIV.NO F. HOGARES", 25, "C", false,277,302);
        String ClteCactEconCiuu = DataUtil.formatearCampo("009309", 6, "C", true,302,308);        
        String ClteCactEconCiuuDes = DataUtil.formatearCampo("OTRAS ACTIVIDADES DE SERV", 25, "C", false,308,333);
        String ClteTclte = DataUtil.formatearCampo("CL", 2, "C", true,333,335);        
        String ClteTclteDes = DataUtil.formatearCampo("CLIENTE", 15, "C", false,335,350);        
        String ClteTbanca = DataUtil.formatearCampo("", 2, "N", true,350,352);        
        String ClteTbancaDes = DataUtil.formatearCampo("", 15, "C", false,352,367);       
        String ClteCsegmentoClte = DataUtil.formatearCampo("", 2, "N", true,367,369);        
        String ClteCsegmentoClteDes = DataUtil.formatearCampo("", 25, "C", false,369,394);        
        String ClteTdcmtoId = DataUtil.formatearCampo("1", 1, "C", true,394,395);        
        String ClteTdcmtoIdDes = DataUtil.formatearCampo("DNI", 15, "C", false,395,410);        
        String ClteNdcmtoId = DataUtil.formatearCampo("000041992043", 12, "C", true,410,422);       
        String ClteAnombRazClte = DataUtil.formatearCampo("RISCO VEGA ANGELA ROCIO", 150, "C", true,422,572);        
        String CLTE_CtipoPersona = DataUtil.formatearCampo("1", 1, "C", true,572,573);          
        String ClteCtipoPersonaDes = DataUtil.formatearCampo("NATURAL", 15, "C", false,573,588);        
        String ClteNtelefono = DataUtil.formatearCampo("01-5192000", 12, "C", true,588,600);         
        String ClteNanexoTelef = DataUtil.formatearCampo("096799", 6, "C", true,600,606);         
        String ClteNfax = DataUtil.formatearCampo("01-7760791", 12, "C", true,606,618);        
        String ClteCagenteBcr = DataUtil.formatearCampo("", 8, "N", true,618,626);        
        String ClteCclteSbs = DataUtil.formatearCampo("0071759717", 10, "C", true,626,636);        
        String ClteCmancomunoSbs = DataUtil.formatearCampo("0000000000", 10, "C", true,636,646);       
        String ClteClasifDeubn = DataUtil.formatearCampo("0", 1, "C", true,646,647);       
        String ClteFiller01 = DataUtil.formatearCampo("", 4, "C", false,647,651);       
        String ClteCclasifDeuSb = DataUtil.formatearCampo("0", 1, "C", true,651,652);       
        String ClteCclasifDeuSbDes = DataUtil.formatearCampo("NORMAL", 15, "C", false,652,667);
        String ClteCmagnitudClte = DataUtil.formatearCampo("0", 1, "C", true,667,668);       		
        String ClteCmagnitudClteDes = DataUtil.formatearCampo("PERSONA NATURAL SIN NEGOC", 25, "C", false,668,693);      
        String ClteGdatoComplem = DataUtil.formatearCampo("P", 1, "C", true,693,694);      
        String ClteCrelLabBco = DataUtil.formatearCampo("", 1, "N", true,694,695);        
        String ClteCaccionistBco = DataUtil.formatearCampo("", 1, "N", true,695,696);        
        String ClteCatrasoDeu = DataUtil.formatearCampo("A", 1, "C", true,696,697);        
        String CLTE_CATRASO_DEU_DES = DataUtil.formatearCampo("CUANDO EL DEUDOR HA PRESE", 25, "C", false,697,722);         
        String CLTE_CRIESG_CAMB = DataUtil.formatearCampo("1", 1, "C", true,722,723);        
        String CLTE_CRIESG_CAMB_DES = DataUtil.formatearCampo("DEUDOR NO EXPUESTO AL RIE", 25, "C", false,723,748);       
        String CLTE_GCHEQ_SFOND_BCO = DataUtil.formatearCampo("0", 1, "C", true,748,749);         
        String CLTE_GCHEQ_SFOND_SBS = DataUtil.formatearCampo("0", 1, "C", true,749,750);         
        String CLTE_GLAVADO_ACTIVO = DataUtil.formatearCampo("0", 1, "C", true,750,751);        
        String CLTE_GCLAVE_INTERNE = DataUtil.formatearCampo("1", 1, "C", true,751,752);        
        String CLTE_GTARJETA_MULTI = DataUtil.formatearCampo("1", 1, "C", true,752,753);        
        String CLTE_GPREST_MULTIR = DataUtil.formatearCampo("0", 1, "C", true,753,754);        
        String CLTE_GGARANTE_MULTI = DataUtil.formatearCampo("0", 1, "C", true,754,755);        
        String CLTE_GVISITA_DOMIC = DataUtil.formatearCampo("0", 1, "C", true,755,756);        
        String CLTE_GNACIONALIDAD = DataUtil.formatearCampo("1", 1, "C", true,756,757);        
        String CLTE_GMENOR_EDAD = DataUtil.formatearCampo("0", 1, "C", true,757,758);        
        String CLTE_GACEPTANTE = DataUtil.formatearCampo("0", 1, "C", true,758,759);        
        String CLTE_GCOMERCIO_EXT = DataUtil.formatearCampo("0", 1, "C", true,759,760);        
        String CLTE_CRESIDENC_PAIS = DataUtil.formatearCampo("1", 1, "C", true,760,761);        
        String CLTE_CRESIDENC_PAIS_DES = DataUtil.formatearCampo("RESIDENTE EN EL PAIS", 25, "C", false,761,786);        
        String CLTE_CPAIS_RESID = DataUtil.formatearCampo("PE", 4, "C", true,786,790);        
        String CLTE_CPAIS_RESID_DES = DataUtil.formatearCampo("PERU", 15, "C", false,790,805);        
        String CLTE_CSECTOR_PUB = DataUtil.formatearCampo("1", 3, "C", true,805,808);        
        String CLTE_CSECTOR_PUB_DES = DataUtil.formatearCampo("", 25, "C", false,808,833);        
        String CLTE_GFALLECIDO = DataUtil.formatearCampo("0", 1, "C", true,833,834);        
        String CLTE_CENTIDAD_INTER = DataUtil.formatearCampo("0000", 4, "C", true,834,838);       
        String CLTE_CENTIDAD_INTER_DES = DataUtil.formatearCampo("BANCO DE LA NACION", 25, "C", false,838,863);       
        String CLTE_CCLIENTE_INTER = DataUtil.formatearCampo("000000", 6, "C", true,863,869);       
        String CLTE_CGRUPO_INTERF = DataUtil.formatearCampo("000", 3, "C", true,869,872);      
        String CLTE_TDCMTO_REPORT = DataUtil.formatearCampo("1", 2, "C", true,872,874);        
        String CLTE_NDCMTO_REPORT = DataUtil.formatearCampo("0041992043", 12, "C", true,874,886);        
        String CLTE_ANOMBRES_REPOR = DataUtil.formatearCampo("RISCO VEGA ANGELA ROCIO", 150, "C", true,886,1036);       
        String CLTE_GINTERDICTO = DataUtil.formatearCampo("0", 1, "C", true,1036,1037);       
        String CLTE_FULT_LAVADO = DataUtil.formatearCampo("", 8, "N", true,1037,1045);         
        String CLTE_CCLIENTE_ANT = DataUtil.formatearCampo("00000000000", 11, "C", true,1045,1056);  
        String  CLTE_CGRUPO_ANT = DataUtil.formatearCampo("000", 3, "C", true,1056,1059);        
        String CLTE_FULTIMA_ACT = DataUtil.formatearCampo("20210820", 8, "C", true,1059,1067);        
        String CLTE_SING_ANUALES = DataUtil.formatearCampo(" 0000000000000.00", 17, "C", true,1067,1084);        
        String CLTE_CCLASIF_DEU_SALIN = DataUtil.formatearCampo("", 1, "N", true,1084,1085);        
        String CLTE_CCLASIF_DEU_ALIN = DataUtil.formatearCampo("", 1, "N", true,1085,1086);        
        String CLTE_TDOC_COMP = DataUtil.formatearCampo("", 1, "N", true,1086,1087);        
        String CLTE_NDOC_COMP = DataUtil.formatearCampo("000000000000", 12, "C", true,1087,1099);        
        String CLTE_TAPERTURA_CLTE = DataUtil.formatearCampo("R", 1, "C", true,1099,1100);        
        String CLTE_GPERS_EXP_PUB = DataUtil.formatearCampo("0", 1, "C", false,1100,1101);        
        String CLTE_GCONTRACTUAL = DataUtil.formatearCampo("1", 1, "C", false,1101,1102);        
        String CLTE_GMENSAJE_TXT = DataUtil.formatearCampo("", 1, "C", false,1102,1103);        
        String CLTE_GOBLIG_INFORMAR = DataUtil.formatearCampo("", 1, "C", false,1103,1104);        
        String FILLER02 = DataUtil.formatearCampo("", 1, "C", false,1104,1105);
        String CLTE_UBI_CLTE = DataUtil.formatearCampo("150141", 6, "C", true,1105,1111);        
        String CLTE_DIR_CLTE = DataUtil.formatearCampo("CA DONATELLO 237 PISO 2 DPTO 201 ALT CDRA 6 Y 7 ARTES NORTE", 70, "C", true,1111,1181);       
        String CLTEN_CIC = DataUtil.formatearCampo("000005243466", 12, "C", false,1181,1193);        
        String CLTEN_APE_PATERNO = DataUtil.formatearCampo("RISCO", 50, "C", false,1193,1243);        
        String CLTEN_APE_MATERNO = DataUtil.formatearCampo("VEGA", 50, "C", true,1243,1293);       
        String CLTEN_APE_DE_CASADA = DataUtil.formatearCampo("", 50, "N", true,1293,1343);       
        String CLTEN_APRIMERO = DataUtil.formatearCampo("ANGELA", 50, "C", true,1343,1393);        
        String CLTEN_ASEGUNDO = DataUtil.formatearCampo("ROCIO", 50, "C", true,1393,1443);        
        String CLTEN_CPAIS_NAC = DataUtil.formatearCampo("PE", 4, "C", false,1443,1447);        
        String CLTEN_CPAIS_NAC_DES = DataUtil.formatearCampo("PERU", 15, "C", true,1447,1462);       
        String CLTEN_FNACIMIENTO = DataUtil.formatearCampo("19830227", 8, "C", true,1462,1470);       
        String CLTEN_TUBIGEO_NAC = DataUtil.formatearCampo("3", 1, "C", true,1470,1471);       
        String CLTEN_CUBIGEO_NAC = DataUtil.formatearCampo("140902", 6, "C", true,1471,1477);        
        String CLTEN_CSEXO = DataUtil.formatearCampo("F", 1, "C", true,1477,1478);        
        String CLTEN_CESTADO_CIVIL = DataUtil.formatearCampo("3", 1, "C", false,1478,1479);       
        String CLTEN_CESTADO_CIVIL_DES = DataUtil.formatearCampo("DIVORCIADO", 15, "C", true,1479,1494);         
        String CLTEN_NTELEFONO_PERS = DataUtil.formatearCampo("1990874808", 12, "C", true,1494,1506);        
        String CLTEN_EMAIL_PERSONAL = DataUtil.formatearCampo("ANGELA.RISCOV@GMAIL.COM", 96, "C", true,1506,1602);        
        String CLTEN_EMAIL_TRABAJO = DataUtil.formatearCampo("ARISCOV@BN.COM.PE", 96, "C", true,1602,1698);       
        String CLTEN_CGRADO_INSTRUC = DataUtil.formatearCampo("", 1, "N", true,1698,1699);       
        String CLTEN_CPROFESION = DataUtil.formatearCampo("", 5, "C", false,1699,1704);       
        String CLTEN_CPROFESION_DES = DataUtil.formatearCampo("", 15, "N", true,1704,1719);
        String CLTEN_COCUPACION = DataUtil.formatearCampo("00015", 5, "C", false,1719,1724);     
        String CLTEN_COCUPACION_DES = DataUtil.formatearCampo("ANALISTA DE SIS", 15, "C", true,1724,1739);      
        String CLTEN_COCUP_REPORT = DataUtil.formatearCampo("ANALISTA DE SISTEM", 18, "C", true,1739,1757);       
        String CLTEN_GPERS_NEGOCIO = DataUtil.formatearCampo("0", 1, "C", true,1757,1758);       
        String CLTEN_NRUC_PERS_NEG = DataUtil.formatearCampo("000000000000", 12, "C", true,1758,1770);        
        String CLTEN_NDEPENDIENTES = DataUtil.formatearCampo("00", 2, "C", true,1770,1772);        
        String CLTEN_ALOCAL_NAC = DataUtil.formatearCampo("", 30, "N", true,1772,1802);        
        String CLTEN_NANOS_EST = DataUtil.formatearCampo("0", 1, "C", true,1802,1803);        
        String CLTEN_NESTATURA = DataUtil.formatearCampo("153", 3, "C", true,1803,1806);        
        String CLTEN_ANOMBRE_PADRE = DataUtil.formatearCampo("CARLOS NESTOR", 15, "C", true,1806,1821);        
        String CLTEN_ANOMBRE_MADRE = DataUtil.formatearCampo("MAURILIA", 15, "C", false,1821,1836);       
        String CLTEJ_CINTERNO_CIC = DataUtil.formatearCampo("000000000000", 12, "C", true,1836,1848);        
        String CLTEJ_ASIGLAS = DataUtil.formatearCampo("", 60, "N", true,1848,1908);       
        String CLTEJ_ARAZON_SOCIAL = DataUtil.formatearCampo("", 150, "N", true,1908,2058);       
        String CLTEJ_FCONSTITUCION = DataUtil.formatearCampo("", 8, "N", true,2058,2066);       
        String CLTEJ_REG_PUB_OFIC = DataUtil.formatearCampo("", 2, "N", true,2066,2068);       
        String CLTEJ_REG_PUB_SUBOFI = DataUtil.formatearCampo("", 2, "N", true,2068,2070);       
        String CLTEJ_REG_PUB_ID = DataUtil.formatearCampo("", 1, "N", true,2070,2071);        
        String CLTEJ_REG_PUB_NPART = DataUtil.formatearCampo("0000000000", 10, "C", true,2071,2081);        
        String CLTEJ_REG_PUB_FOLIO = DataUtil.formatearCampo("000", 3, "C", true,2081,2084);        
        String CLTEJ_REG_PUB_TOMO = DataUtil.formatearCampo("0000000", 7, "C", true,2084,2091);        
        String CLTEJ_REG_PUB_REPORT = DataUtil.formatearCampo("", 20, "N", true,2091,2111);        
        String CLTEJ_DPAGINA_WEB = DataUtil.formatearCampo("", 96, "C", false,2111,2207);       
        String CLTEJ_NTELEFONO_CELULAR = DataUtil.formatearCampo("", 12, "N", true,2207,2219);        
        String CLTEJ_EMAIL_EMPRESA = DataUtil.formatearCampo("", 96, "N", true,2219,2315);        
        String CLTEJ_EMAIL_OPCIONAL = DataUtil.formatearCampo("", 96, "C", false,2315,2411);        
        String CLTEJ_FILLER = DataUtil.formatearCampo("", 4, "N", true,2411,2415);        
        String CLTEDIR_CINTERNO_CIC = DataUtil.formatearCampo("000005243466", 12, "C", true,2415,2427);        
        String CLTEDIR_NITEM_DIREC = DataUtil.formatearCampo("00004", 5, "C", true,2427,2432);        
        String CLTEDIR_TDIRECCION = DataUtil.formatearCampo("8", 2, "C", true,2432,2434);        
        String CLTEDIR_TVIA = DataUtil.formatearCampo(2, 1, "N", true,2434,2435);        
        String CLTEDIR_AVIA = DataUtil.formatearCampo("DONATELLO", 60, "C", true,2435,2495);       
        String CLTEDIR_NNUMERO = DataUtil.formatearCampo("237", 30, "C", true,2495,2525);        
        String CLTEDIR_NBLOQUE = DataUtil.formatearCampo("", 30, "N", true,2525,2555);        
        String CLTEDIR_NMANZANA = DataUtil.formatearCampo("", 30, "N", true,2555,2585);        
        String CLTEDIR_NLOTE = DataUtil.formatearCampo("", 30, "N", true,2585,2615);        
        String CLTEDIR_NPISO = DataUtil.formatearCampo("2", 30, "C", true,2615,2645);        
        String CLTEDIR_NDPTO_INTER = DataUtil.formatearCampo("201", 30, "C", true,2645,2675);        
        String CLTEDIR_DREPORTADA = DataUtil.formatearCampo("CALLE DONATELLO 237 PI 2 DP.INT.201 ALT CDRA 6 Y 7 ARTES NORTE", 70, "C", true,2675,2745);        
        String CLTEDIR_APTO_REFEREN = DataUtil.formatearCampo("ALT CDRA 6 Y 7 ARTES NORTE", 96, "C", true,2745,2841);        
        String CLTEDIR_NAPARTADO_POST = DataUtil.formatearCampo("", 25, "N", true,2841,2866);        
        String CLTEDIR_NTELEFONO_DIR = DataUtil.formatearCampo("000000000000", 12, "C", true,2866,2878);        
        String CLTEDIR_NANEXO_DIR = DataUtil.formatearCampo("000000", 6, "C", true,2878,2884);       
        String CLTEDIR_NFAX = DataUtil.formatearCampo("000000000000", 12, "C", true,2884,2896);       
        String CLTEDIR_TUBIGEO = DataUtil.formatearCampo("1", 1, "C", true,2896,2897);        
        String CLTEDIR_CUBIGEO = DataUtil.formatearCampo("150117", 6, "C", true,2897,2903);       
        String CLTEDIR_GVISITA = DataUtil.formatearCampo("N", 1, "C", true,2903,2904);        
        String CLTEDIR_FVISITA = DataUtil.formatearCampo("00000000", 8, "C", true,2904,2912);        
        String CLTEDIR_AVISITA = DataUtil.formatearCampo("", 150, "N", true,2912,3062);         
        String CLTEDIR_CPOSTAL = DataUtil.formatearCampo("0022052548", 15, "C", true,3062,3077);       
        String CLTEDIR_CUBIGEO_AHORROS = DataUtil.formatearCampo("150130", 6, "C", true,3077,3083);         
        String CLTEDIR_CUBIGEO_CTA_CTE = DataUtil.formatearCampo("150141", 6, "C", true,3083,3089);       
        String CLTEDIR_CUBIGEO_RENIEC = DataUtil.formatearCampo("", 6, "N", true,3089,3095);        
        String CLTEDIR_CUBIGEO_SUNAT = DataUtil.formatearCampo("", 6, "N", true,3095,3101);      
        String CLTEDIR_FINGRESO_DIR = DataUtil.formatearCampo("00000000", 8, "C", true,3101,3109);       
        String CLTEDIR_FULTMODIF_DIR = DataUtil.formatearCampo("20171020", 8, "C", true,3109,3117);       
        String CLTEDIR_BSITUACION = DataUtil.formatearCampo("1", 1, "C", false,3117,3118);        
        String CLTEDIR_FILLER = DataUtil.formatearCampo("", 59, "C", false,3118,3177);
        String CLTEIND_CINTERNO_CIC = DataUtil.formatearCampo("000005243466", 12, "C", false,3177,3189);        
        String CLTEIND_CESTADO_CVC = DataUtil.formatearCampo("", 1, "C", false,3189,3190);         
        String CLTEIND_CRAZON_ALTA = DataUtil.formatearCampo("", 3, "C", false,3190,3193);        
        String CLTEIND_CSITC_ECON = DataUtil.formatearCampo("", 2, "C", false,3193,3195);        
        String CLTEIND_FPRIM_ACRDO = DataUtil.formatearCampo("", 8, "C", false,3195,3203);        
        String CLTEIND_GPROTEC_DATOS = DataUtil.formatearCampo("", 1, "C", false,3203,3204);        
        String CLTEIND_GNO_PUBLICIDAD = DataUtil.formatearCampo("", 1, "C", false,3204,3205);         
        String CLTEIND_GSESION_TERC = DataUtil.formatearCampo("", 1, "C", false,3205,3206);        
        String CLTEIND_GACCESO_RSTRG = DataUtil.formatearCampo("", 1, "C", false,3206,3207);        
        String CLTEIND_GVIP = DataUtil.formatearCampo("0", 1, "C", false,3207,3208);        
        String CLTEIND_GLST_NGR = DataUtil.formatearCampo("", 1, "C", false,3208,3209);        
        String CLTEIND_TRAT_ESP = DataUtil.formatearCampo("D", 1, "C", false,3209,3210);        
        String CLTEIND_BLQ_CL = DataUtil.formatearCampo("", 1, "C", false,3210,3211);         
        String CLTEIND_GTOCKEN = DataUtil.formatearCampo("", 1, "C", false,3211,3212);        
        String CLTEIND_GBCELULAR = DataUtil.formatearCampo("", 1, "C", false,3212,3213);       
        String CLTE_FECHA_PACTO = DataUtil.formatearCampo("", 8, "C", false,3213,3221);        
        String CLTE_ADEPARTAMENTO_AHOR = DataUtil.formatearCampo("LIMA", 50, "C", false,3221,3271);        
        String CLTE_APROVINCIA_AHOR = DataUtil.formatearCampo("LIMA", 50, "C", false,3271,3321);       
        String CLTE_ADISTRITO_AHOR = DataUtil.formatearCampo("SAN BORJA", 50, "C", false,3321,3371);        
        String CLTE_ADEPARTAMENTO_CTCT = DataUtil.formatearCampo("LIMA", 50, "C", false,3371,3421);          
        String CLTE_APROVINCIA_CTCT = DataUtil.formatearCampo("LIMA", 50, "C", false,3421,3471);         
        String CLTE_ADISTRITO_CTCT = DataUtil.formatearCampo("SAN BORJA", 50, "C", false,3471,3521);        
        String CLTE_FILLER_DIR = DataUtil.formatearCampo("", 6690, "C", false,3521,10211);        

        // Construir la cadena con los campos formateados
        StringBuilder tramaFormateada = new StringBuilder();
        tramaFormateada.append(hdLong).append(hdTran).append(hdTimestamp).append(hdCodTran1Format).
        append(hdCodUser).append(hdDesRestFormat).append(HD_DES_RET).append(hdCodCanal).append(hdCodTerm).append(hdCodRetLog).
        append(hdDesRetLog).append(inTdcmtoId).append(inNdcmtoIdFormat).append(inTipoBusFormat).append(inTipoResultFormat).
        append(inUltctaFormat).append(inCicFormat).append(inCodProdFormat).append(inNumProdFormat).append(inNumCorDirFormat).
        append(inNtarjetaInputFormat).append(inTdocumAdFormat).append(inFiller).append(ClteCinternoCic).append(ClteCoficIngreso).
        append(ClteCoficOperac).append(ClteFingresoBco).append(ClteCejectNeg).append(des).append(ClteCsectorEcon).
        append(ClteCsectorEconDes).append(ClteCactEconCiuu).append(ClteCactEconCiuuDes).append(ClteTclte).append(ClteTclteDes).
        append(ClteTbanca).append(ClteTbancaDes).append(ClteCsegmentoClte).append(ClteCsegmentoClteDes).append(ClteTdcmtoId).
        append(ClteTdcmtoIdDes).append(ClteNdcmtoId).append(ClteAnombRazClte).append(CLTE_CtipoPersona).append(ClteCtipoPersonaDes).
        append(ClteNtelefono).append(ClteNanexoTelef).append(ClteNfax).append(ClteCagenteBcr).append(ClteCclteSbs).
        append(ClteCmancomunoSbs).append(ClteClasifDeubn).append(ClteFiller01).append(ClteCclasifDeuSb).append(ClteCclasifDeuSbDes).
        append(ClteCmagnitudClte).append(ClteCmagnitudClteDes).append(ClteGdatoComplem).append(ClteCrelLabBco).append(ClteCaccionistBco).
        append(ClteCatrasoDeu).append(CLTE_CATRASO_DEU_DES).append(CLTE_CRIESG_CAMB).append(CLTE_CRIESG_CAMB_DES).
        append(CLTE_GCHEQ_SFOND_BCO).append(CLTE_GCHEQ_SFOND_SBS).append(CLTE_GLAVADO_ACTIVO).append(CLTE_GCLAVE_INTERNE).
        append(CLTE_GTARJETA_MULTI).append(CLTE_GPREST_MULTIR).append(CLTE_GGARANTE_MULTI).append(CLTE_GVISITA_DOMIC).append(CLTE_GNACIONALIDAD).
        append(CLTE_GMENOR_EDAD).append(CLTE_GACEPTANTE).append(CLTE_GCOMERCIO_EXT).append(CLTE_CRESIDENC_PAIS).append(CLTE_CRESIDENC_PAIS_DES).
        append(CLTE_CPAIS_RESID).append(CLTE_CPAIS_RESID_DES).append(CLTE_CSECTOR_PUB).append(CLTE_CSECTOR_PUB_DES).append(CLTE_GFALLECIDO).
        append(CLTE_CENTIDAD_INTER).append(CLTE_CENTIDAD_INTER_DES).append(CLTE_CCLIENTE_INTER).append(CLTE_CGRUPO_INTERF).
        append(CLTE_TDCMTO_REPORT).append(CLTE_NDCMTO_REPORT).append(CLTE_ANOMBRES_REPOR).append(CLTE_GINTERDICTO).append(CLTE_FULT_LAVADO).
        append(CLTE_CCLIENTE_ANT).append(CLTE_CGRUPO_ANT).append(CLTE_FULTIMA_ACT).append(CLTE_SING_ANUALES).append(CLTE_CCLASIF_DEU_SALIN).
        append(CLTE_CCLASIF_DEU_ALIN).append(CLTE_TDOC_COMP).append(CLTE_NDOC_COMP).append(CLTE_TAPERTURA_CLTE).append(CLTE_GPERS_EXP_PUB).
        append(CLTE_GCONTRACTUAL).append(CLTE_GMENSAJE_TXT).append(CLTE_GOBLIG_INFORMAR).append(FILLER02).append(CLTE_UBI_CLTE).
        append(CLTE_DIR_CLTE).append(CLTEN_CIC).append(CLTEN_APE_PATERNO).append(CLTEN_APE_MATERNO).append(CLTEN_APE_DE_CASADA).append(CLTEN_APRIMERO).
        append(CLTEN_ASEGUNDO).append(CLTEN_CPAIS_NAC).append(CLTEN_CPAIS_NAC_DES).append(CLTEN_FNACIMIENTO).append(CLTEN_TUBIGEO_NAC).
        append(CLTEN_CUBIGEO_NAC).append(CLTEN_CSEXO).append(CLTEN_CESTADO_CIVIL).append(CLTEN_CESTADO_CIVIL_DES).append(CLTEN_NTELEFONO_PERS).
        append(CLTEN_EMAIL_PERSONAL).append(CLTEN_EMAIL_TRABAJO).append(CLTEN_CGRADO_INSTRUC).append(CLTEN_CPROFESION).append(CLTEN_CPROFESION_DES).
        append(CLTEN_COCUPACION).append(CLTEN_COCUPACION_DES).append(CLTEN_COCUP_REPORT).append(CLTEN_GPERS_NEGOCIO).append(CLTEN_NRUC_PERS_NEG).
        append(CLTEN_NDEPENDIENTES).append(CLTEN_ALOCAL_NAC).append(CLTEN_NANOS_EST).append(CLTEN_NESTATURA).append(CLTEN_ANOMBRE_PADRE).
        append(CLTEN_ANOMBRE_MADRE).append(CLTEJ_CINTERNO_CIC).append(CLTEJ_ASIGLAS).append(CLTEJ_ARAZON_SOCIAL).append(CLTEJ_FCONSTITUCION).append(CLTEJ_REG_PUB_OFIC).
        append(CLTEJ_REG_PUB_SUBOFI).append(CLTEJ_REG_PUB_ID).append(CLTEJ_REG_PUB_NPART).append(CLTEJ_REG_PUB_FOLIO).append(CLTEJ_REG_PUB_TOMO).
        append(CLTEJ_REG_PUB_REPORT).append(CLTEJ_DPAGINA_WEB).append(CLTEJ_NTELEFONO_CELULAR).append(CLTEJ_EMAIL_EMPRESA).append(CLTEJ_EMAIL_OPCIONAL).
        append(CLTEJ_FILLER).append(CLTEDIR_CINTERNO_CIC).append(CLTEDIR_NITEM_DIREC).append(CLTEDIR_TDIRECCION).append(CLTEDIR_TVIA).
        append(CLTEDIR_AVIA).append(CLTEDIR_NNUMERO).append(CLTEDIR_NBLOQUE).append(CLTEDIR_NMANZANA).append(CLTEDIR_NLOTE).append(CLTEDIR_NPISO).
        append(CLTEDIR_NDPTO_INTER).append(CLTEDIR_DREPORTADA).append(CLTEDIR_APTO_REFEREN).append(CLTEDIR_NAPARTADO_POST).append(CLTEDIR_NTELEFONO_DIR).
        append(CLTEDIR_NANEXO_DIR).append(CLTEDIR_NFAX).append(CLTEDIR_TUBIGEO).append(CLTEDIR_CUBIGEO).append(CLTEDIR_GVISITA).append(CLTEDIR_FVISITA).
        append(CLTEDIR_AVISITA).append(CLTEDIR_CPOSTAL).append(CLTEDIR_CUBIGEO_AHORROS).append(CLTEDIR_CUBIGEO_CTA_CTE).append(CLTEDIR_CUBIGEO_RENIEC).append(CLTEDIR_CUBIGEO_SUNAT).
        append(CLTEDIR_FINGRESO_DIR).append(CLTEDIR_FULTMODIF_DIR).append(CLTEDIR_BSITUACION).append(CLTEDIR_FILLER).append(CLTEIND_CINTERNO_CIC).
        append(CLTEIND_CESTADO_CVC).append(CLTEIND_CRAZON_ALTA).append(CLTEIND_CSITC_ECON).append(CLTEIND_FPRIM_ACRDO).append(CLTEIND_GPROTEC_DATOS).append(CLTEIND_GNO_PUBLICIDAD).
        append(CLTEIND_GSESION_TERC).append(CLTEIND_GACCESO_RSTRG).append(CLTEIND_GVIP).append(CLTEIND_GLST_NGR).append(CLTEIND_TRAT_ESP).append(CLTEIND_BLQ_CL).
        append(CLTEIND_GTOCKEN).append(CLTEIND_GBCELULAR).append(CLTE_FECHA_PACTO).append(CLTE_ADEPARTAMENTO_AHOR).append(CLTE_APROVINCIA_AHOR).
        append(CLTE_ADISTRITO_AHOR).append(CLTE_ADEPARTAMENTO_CTCT).append(CLTE_APROVINCIA_CTCT).append(CLTE_ADISTRITO_CTCT).append(CLTE_FILLER_DIR);
        
        return tramaFormateada.toString() + "*";
    }
    
    
}
