package pe.com.bn.ms.service.customer;

import org.springframework.http.HttpHeaders;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.dto.ClienteContactabilidad;
import pe.com.bn.ms.dto.Customer;
import pe.com.bn.ms.dto.CustomerCtaSIDG;
import pe.com.bn.ms.dto.CustomerSIDG;

/**
 * @author prov_destrada
 * @created 21/10/2020 - 05:35 p. m.
 * @project ms-dataclients
 */
public interface CustomerCreateService
{
    // Registrar nuevo cliente
    GenericResponse<?> createNewCustomer(Customer request, HttpHeaders httpHeaders)
    throws Throwable;
    
 // Registrar nuevo cliente
    GenericResponse<?> createNewCustomerSIDG(CustomerSIDG request, HttpHeaders httpHeaders)
    throws Throwable;
    
    // Registrar nuevo cliente
    GenericResponse<?> createNewCustomerCtaSIDG(CustomerCtaSIDG request, HttpHeaders httpHeaders)
    throws Throwable;    

    // Actualizar información de cliente
    GenericResponse<?> createNewCustomerContactInformation(ClienteContactabilidad clienteContactabilidad,
                                                           HttpHeaders httpHeaders) throws Throwable;
}
