package pe.com.bn.ms.service.address;

import pe.com.bn.ms.dto.Direccion;
import pe.com.bn.ms.dto.PersonaCliente;


public interface AddressService
{
    Direccion findAddressByCic(String CIC) throws Exception;

    Direccion findAddressByCicAndCorrelative(String CIC, String numCorre) throws Exception;

    Direccion createClientAddress(PersonaCliente personaCliente) throws Exception;

    Direccion updateClientAddress(PersonaCliente personaCliente) throws Exception;

}
