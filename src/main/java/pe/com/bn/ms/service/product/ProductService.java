package pe.com.bn.ms.service.product;

import org.springframework.http.HttpHeaders;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.domain.TypeSearchProduct;
import pe.com.bn.ms.domain.response.DataProduct.CustomProductResponse;

/**
 * @author prov_destrada
 * @created 22/10/2020 - 06:13 p. m.
 * @project ms-dataclients
 */
public interface ProductService
{
    // Buscar producto por 'tipo y número de documento'
    GenericResponse<CustomProductResponse> findProductByTypeAndNumberDocument(HttpHeaders httpHeaders, TypeSearchProduct request) throws Throwable;

    // Buscar producto por 'cic'
    GenericResponse<CustomProductResponse> findProductByCic(HttpHeaders httpHeaders, TypeSearchProduct request) throws Throwable;
    
    // Buscar producto por tipo y numero de producto
    GenericResponse<CustomProductResponse> findCustomerByTypeAndNumberProduct(HttpHeaders httpHeaders, TypeSearchProduct request) throws Throwable;
}
