package pe.com.bn.ms.service.product;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.domain.TypeSearchProduct;
import pe.com.bn.ms.domain.response.DataProduct.CustomProductResponse;
import pe.com.bn.ms.dto.GatewayRequest;
import pe.com.bn.ms.dto.GatewayResponse;
import pe.com.bn.ms.dto.Product;
import pe.com.bn.ms.payload.GatewayClient;
import pe.com.bn.ms.service.ObjectConversionService;

import static pe.com.bn.ms.util.PayloadValidator.httpHeadersForGateway;
import static pe.com.bn.ms.util.PayloadValidator.validateGatewayResponse;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_CODE_TRANSACTION_612;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_COD_USER;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_LENGTH;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_TRANSACTION_PC01;
import static pe.com.bn.ms.util.enums.PlotParameters.OPERATION_READ;
import static pe.com.bn.ms.util.enums.TypeResult.RESULT_PRODUCT;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_CIC;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_NUMBER_DOCUMENT;
import static pe.com.bn.ms.util.functions.DateTimePicker.gatewayTimestamp;
import static pe.com.bn.ms.util.functions.ReflectionsUtil.validatePlotResponse;

/**
 * @author prov_destrada
 * @created 22/10/2020 - 06:14 p. m.
 * @project ms-dataclients
 */
@Service
public class ProductServiceImpl implements ProductService
{
    private final ObjectConversionService objectConversionService;
    private final GatewayClient           gatewayClient;

    @Autowired
    public ProductServiceImpl(ObjectConversionService objectConversionService, GatewayClient gatewayClient)
    {
        this.objectConversionService = objectConversionService;
        this.gatewayClient           = gatewayClient;
    }

    /**
     * =================================================================================================================
     *
     * @param httpHeaders cabeceras de la aplicación
     * @param request     {@link TypeSearchProduct}
     * @return {@link Product}
     * @throws Throwable error
     */
    @Override
    public GenericResponse<CustomProductResponse> findProductByTypeAndNumberDocument(HttpHeaders httpHeaders, TypeSearchProduct request)
    throws Throwable
    {
//        Product product = new Product();
//        // Setear headers
//        product.setHdLong(HEADER_LENGTH.getValue());
//        product.setHdTran(HEADER_TRANSACTION_PC01.getValue());
//        product.setHdCodTran(HEADER_CODE_TRANSACTION_612.getValue());
//        product.setHdTimestamp(gatewayTimestamp());
//        product.setHdCodUser(HEADER_COD_USER.getValue());
//        // Setear body
//        product.setTypeSearch(SEARCH_NUMBER_DOCUMENT.getSearch());
//        product.setTypeResult(RESULT_PRODUCT.getResult());
//        product.setTypeDocumentIn(request.getTypeDocument());
//        product.setNumberDocumentIn(request.getNumberDocument());
//
//        // ----------------------------------------------
//        //
//        GatewayRequest gwRequest = new GatewayRequest();
//        gwRequest.setLongitud(product.getHdLong());
//        gwRequest.setTransId(product.getHdTran());
//
//        gwRequest = objectConversionService
//                .convertDtoToPlot(product, "", OPERATION_READ.getValue(), gwRequest);
//
//        ResponseEntity<GatewayResponse> gwResponseEnt = gatewayClient
//                .runGatewayOperation(httpHeadersForGateway(httpHeaders), gwRequest);
//
//        // Validar la respuesta
//        GatewayResponse gwResponse = validateGatewayResponse(gwResponseEnt);
//
//        // Convertir en objeto
//        product = objectConversionService
//                .convertPlotToDto(new Product(), gwResponse.getDatos(), "");
//
//        validatePlotResponse(product);

        //return new GenericResponse<CustomProductResponse>(product);
        return new GenericResponse<CustomProductResponse>();
    }

    /**
     * =================================================================================================================
     * Buscar producto por código único de cliente
     *
     * @param httpHeaders cabeceras de la aplicación
     * @param request     {@link TypeSearchProduct}
     * @return {@link Product}
     * @throws Throwable error
     */
    @Override
    public GenericResponse<CustomProductResponse> findProductByCic(HttpHeaders httpHeaders, TypeSearchProduct request) throws Throwable
    {
//        Product product = new Product();
//        // Setear headers
//        product.setHdLong(HEADER_LENGTH.getValue());
//        product.setHdTran(HEADER_TRANSACTION_PC01.getValue());
//        product.setHdCodTran(HEADER_CODE_TRANSACTION_612.getValue());
//        product.setHdTimestamp(gatewayTimestamp());
//        product.setHdCodUser(HEADER_COD_USER.getValue());
//        // Setear body
//        product.setTypeSearch(SEARCH_CIC.getSearch());
//        product.setTypeResult(RESULT_PRODUCT.getResult());
//        product.setCicInSecond(request.getCic());
//
//        // ----------------------------------------------
//        //
//        GatewayRequest gwRequest = new GatewayRequest();
//        gwRequest.setLongitud(product.getHdLong());
//        gwRequest.setTransId(product.getHdTran());
//
//        gwRequest = objectConversionService
//                .convertDtoToPlot(product, "", OPERATION_READ.getValue(), gwRequest);
//
//        ResponseEntity<GatewayResponse> gwResponseEnt = gatewayClient
//                .runGatewayOperation(httpHeadersForGateway(httpHeaders), gwRequest);
//
//        // Validar la respuesta
//        GatewayResponse gwResponse = validateGatewayResponse(gwResponseEnt);
//
//        // Convertir en objeto
//        product = objectConversionService
//                .convertPlotToDto(new Product(), gwResponse.getDatos(), "");
//
//        validatePlotResponse(product);

        //return new GenericResponse<CustomProductResponse>(product);
        return new GenericResponse<CustomProductResponse>();
    }

	@Override
	public GenericResponse<CustomProductResponse> findCustomerByTypeAndNumberProduct(HttpHeaders httpHeaders, TypeSearchProduct request)
			throws Throwable {
		// TODO Auto-generated method stub
		return new GenericResponse<CustomProductResponse>();
	}
}
