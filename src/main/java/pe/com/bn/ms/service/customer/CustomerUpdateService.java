package pe.com.bn.ms.service.customer;

import org.springframework.http.HttpHeaders;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.dto.PersonaCliente;

/**
 * @author prov_destrada
 * @created 21/10/2020 - 05:36 p. m.
 * @project ms-dataclients
 */
public interface CustomerUpdateService
{
    GenericResponse<?> updateClientByCIC(PersonaCliente personaCliente, HttpHeaders httpHeaders) throws Throwable;
}
