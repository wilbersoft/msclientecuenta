package pe.com.bn.ms.service.customer;

import org.springframework.http.HttpHeaders;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.domain.TypeSearchCustomer;
import pe.com.bn.ms.domain.TypeSearchCustomerContactability;
import pe.com.bn.ms.domain.response.DataClient.CustomResponse;
import pe.com.bn.ms.dto.PersonaCliente;

/**
 * @author prov_destrada
 * @created 21/10/2020 - 05:36 p. m.
 * @project ms-dataclients
 */
public interface CustomerReadService
{
    // Búsqueda de cliente por 'cic'
	GenericResponse<CustomResponse> findCustomerByCic(HttpHeaders httpHeaders, TypeSearchCustomer request) throws Throwable;

	//GenericResponse<PersonaCliente> findCustomerByCic(HttpHeaders httpHeaders, TypeSearchCustomer request) throws Throwable;

//
    // Búsqueda de cliente por 'tipo y número de documento'
	GenericResponse<CustomResponse> findCustomerByTypeAndNumberDocument(HttpHeaders httpHeaders,
                                                           TypeSearchCustomer request) throws Throwable;

    // Búsqueda de cliente por 'tipo y número de producto'
	GenericResponse<CustomResponse> findCustomerByTypeAndNumberProduct(HttpHeaders httpHeaders,
                                                          TypeSearchCustomer request) throws Throwable;

    // Búsqueda de cliente contactabilidad por 'cic y número de producto'
	GenericResponse<CustomResponse> findCustomerContactInformationByCic(HttpHeaders httpHeaders,
                                                           TypeSearchCustomerContactability request) throws Throwable;

	GenericResponse<CustomResponse> findCustomerSIDGByTypeAndNumberDocument(HttpHeaders httpHeaders,
			TypeSearchCustomer clientRequest) throws Throwable;

}
