package pe.com.bn.ms.service.customer.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import pe.com.bn.ms.datos.TestDatos;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.domain.TypeSearchCustomer;
import pe.com.bn.ms.domain.TypeSearchCustomerContactability;
import pe.com.bn.ms.domain.response.DataClient.*;
import pe.com.bn.ms.dto.GatewayRequest;
import pe.com.bn.ms.dto.GatewayResponse;
import pe.com.bn.ms.dto.PersonaCliente;
import pe.com.bn.ms.payload.GatewayClient;
import pe.com.bn.ms.service.ObjectConversionService;
import pe.com.bn.ms.service.customer.CustomerReadService;

import static pe.com.bn.ms.util.PayloadValidator.httpHeadersForGateway;
import static pe.com.bn.ms.util.PayloadValidator.validateGatewayResponse;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_CODE_TRANSACTION_612;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_LENGTH;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER_TRANSACTION_PC01;
import static pe.com.bn.ms.util.enums.PlotParameters.OPERATION_READ;
import static pe.com.bn.ms.util.enums.TypeResult.RESULT_CUSTOMER;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_CIC;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_NUMBER_DOCUMENT;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_NUMBER_PRODUCT;
import static pe.com.bn.ms.util.enums.TypeSearch.SEARCH_NUMBER_DOCUMENT_SIDG;
import static pe.com.bn.ms.util.functions.ReflectionsUtil.validatePlotResponse;

/**
 * @author prov_destrada
 * @created 21/10/2020 - 05:12 p. m.
 * @project ms-dataclients
 */
@Service
public class CustomerReadServiceImpl implements CustomerReadService
{
    private final ObjectConversionService objectConversionService;
    private final GatewayClient           gatewayClient;

    @Autowired
    public CustomerReadServiceImpl(ObjectConversionService objectConversionService,
                                   GatewayClient gatewayClient)
    {
        this.objectConversionService = objectConversionService;
        this.gatewayClient           = gatewayClient;
    }

    /**
     * =================================================================================================================
     * Búsqueda de cliente por código único de cliente (CIC)
     *
     * @param httpHeaders cabeceras de la aplicación
     * @param request     {@link TypeSearchCustomer}
     * @return {@link PersonaCliente}
     * @throws Exception error
     */
    @Override
    public GenericResponse<CustomResponse> findCustomerByCic(HttpHeaders httpHeaders, TypeSearchCustomer request) throws Throwable
    //public GenericResponse<PersonaCliente> findCustomerByCic(HttpHeaders httpHeaders, TypeSearchCustomer request) throws Throwable
    {

        PersonaCliente customer = new PersonaCliente(httpHeaders);
        // Setear headers
        customer.setHdTran(HEADER_TRANSACTION_PC01.getValue());
        customer.setHdCodTran(HEADER_CODE_TRANSACTION_612.getValue());
        customer.setHdLong(HEADER_LENGTH.getValue());
        customer.setTipoBus(SEARCH_CIC.getSearch());
        customer.setTipoResult(RESULT_CUSTOMER.getResult());
        // Setear body
        customer.setCicInSecond(request.getCic());

        // ----------------------------------------------
        // Generar el request body gateway
        GatewayRequest gwRequest = new GatewayRequest();
        gwRequest.setLongitud(customer.getHdLong());
        gwRequest.setTransId(customer.getHdTran());

        gwRequest = objectConversionService
                .convertDtoToPlot(customer, "", OPERATION_READ.getValue(), gwRequest);

        ResponseEntity<GatewayResponse> gwResponseEnt = gatewayClient
                .runGatewayOperation(httpHeadersForGateway(httpHeaders), gwRequest);

        // Validar la respuesta
        GatewayResponse gwResponse = validateGatewayResponse(gwResponseEnt);

        // Convertir en objeto
        GenericResponse genericResponse=new GenericResponse();
        genericResponse.setData(datosCustomer(gwResponse.getDatos()));

        //String cadena = "9999PC012023-04-20-00-00-00-9999990612DJRS  00000OPERACION OK                                                   000000000000020100000000052434660000000000000000000000000000000000000000000                        0000052434660026000020081110                               001420SECT.PRIV.NO F. HOGARES  009309OTRAS ACTIVIDADES DE SERVCLCLIENTE                                                    1DNI            000041992043RISCO VEGA ANGELA ROCIO                                                                                                                               1NATURAL        01-5192000  96799 01-7760791          007175971700000000000    0NORMAL         0PERSONA NATURAL SIN NEGOCP  ACUANDO EL DEUDOR HA PRESE1DEUDOR NO EXPUESTO AL RIE0001100010001RESIDENTE EN EL PAIS     PE  PERU           1                           00000BANCO DE LA NACION       0000000001 0041992043  RISCO VEGA ANGELA ROCIO                                                                                                                               0        0000000000000020210820 0000000000000.00   000000000000R01   150141CA DONATELLO 237 PISO 2 DPTO 201 ALT CDRA 6 Y 7 ARTES NORTE           000005243466RISCO                                             VEGA                                                                                                ANGELA                                            ROCIO                                             PE  PERU           198302273140902F3DIVORCIADO     1990874808  ANGELA.RISCOV@GMAIL.COM                                                                         ARISCOV@BN.COM.PE                                                                                                    00015ANALISTA DE SISANALISTA DE SISTEM000000000000000                              0153CARLOS NESTOR  MAURILIA       000000000000                                                                                                                                                                                                                               00000000000000000000                                                                                                                                                                                                                                                                                                                                    000005243466000048 2DONATELLO                                                   237                                                                                                                     2                             201                           CALLE DONATELLO 237 PI 2 DP.INT.201 ALT CDRA 6 Y 7 ARTES NORTE        ALT CDRA 6 Y 7 ARTES NORTE                                                                                               0000000000000000000000000000001150117N00000000                                                                                                                                                      0022052548     150130150141            00000000201710201                                                           000005243466                  0 D           LIMA                                              LIMA                                              SAN BORJA                                         LIMA                                              LIMA                                              SAN BORJA                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  *";
        //genericResponse.setData(datosCustomer(cadena));
        //genericResponse.setCodResult("00000");

    	return genericResponse;
    }

    /***
     * Datos de ClienteGeneral_O
     */
    public CustomResponse datosCustomer(String cadena){
        //String cadena = "9999PC012023-04-20-00-00-00-9999990612DJRS  00000OPERACION OK                                                   000000000000020100000000052434660000000000000000000000000000000000000000000                        0000052434660026000020081110                               001420SECT.PRIV.NO F. HOGARES  009309OTRAS ACTIVIDADES DE SERVCLCLIENTE                                                    1DNI            000041992043RISCO VEGA ANGELA ROCIO                                                                                                                               1NATURAL        01-5192000  96799 01-7760791          007175971700000000000    0NORMAL         0PERSONA NATURAL SIN NEGOCP  ACUANDO EL DEUDOR HA PRESE1DEUDOR NO EXPUESTO AL RIE0001100010001RESIDENTE EN EL PAIS     PE  PERU           1                           00000BANCO DE LA NACION       0000000001 0041992043  RISCO VEGA ANGELA ROCIO                                                                                                                               0        0000000000000020210820 0000000000000.00   000000000000R01   150141CA DONATELLO 237 PISO 2 DPTO 201 ALT CDRA 6 Y 7 ARTES NORTE           000005243466RISCO                                             VEGA                                                                                                ANGELA                                            ROCIO                                             PE  PERU           198302273140902F3DIVORCIADO     1990874808  ANGELA.RISCOV@GMAIL.COM                                                                         ARISCOV@BN.COM.PE                                                                                                    00015ANALISTA DE SISANALISTA DE SISTEM000000000000000                              0153CARLOS NESTOR  MAURILIA       000000000000                                                                                                                                                                                                                               00000000000000000000                                                                                                                                                                                                                                                                                                                                    000005243466000048 2DONATELLO                                                   237                                                                                                                     2                             201                           CALLE DONATELLO 237 PI 2 DP.INT.201 ALT CDRA 6 Y 7 ARTES NORTE        ALT CDRA 6 Y 7 ARTES NORTE                                                                                               0000000000000000000000000000001150117N00000000                                                                                                                                                      0022052548     150130150141            00000000201710201                                                           000005243466                  0 D           LIMA                                              LIMA                                              SAN BORJA                                         LIMA                                              LIMA                                              SAN BORJA                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  *";
        CustomResponse customResponse=new CustomResponse();
        //Tipo de Persona 1=Natural| 3=Juridica
        customResponse.setClientGeneral(new TestDatos().getDatosDataClientGeneral(cadena));
        if(customResponse.getClientGeneral().getPersonType().equals("3"))
            customResponse.setClientLegalEntity(new TestDatos().getDataClientLegalEntity(cadena));
        else
            customResponse.setClientNaturalPerson(new TestDatos().getDataClientNaturalPerson(cadena));


        return customResponse;
    }


    /**
     * =================================================================================================================
     * Búsqueda de cliente por tipo y número de documento
     *
     * @param httpHeaders cabeceras de la aplicación
     * @param request     {@link TypeSearchCustomer}
     * @return {@link PersonaCliente}
     * @throws Throwable error
     */
    @Override
    public GenericResponse<CustomResponse> findCustomerByTypeAndNumberDocument(HttpHeaders httpHeaders,
                                                                  TypeSearchCustomer request) throws Throwable
    {

        GenericResponse genericResponse=new GenericResponse();
        String cadena = "9999PC012023-04-20-00-00-00-9999990612DJRS  00000OPERACION OK                                                   000000000000020100000000052434660000000000000000000000000000000000000000000                        0000052434660026000020081110                               001420SECT.PRIV.NO F. HOGARES  009309OTRAS ACTIVIDADES DE SERVCLCLIENTE                                                    1DNI            000041992043RISCO VEGA ANGELA ROCIO                                                                                                                               1NATURAL        01-5192000  96799 01-7760791          007175971700000000000    0NORMAL         0PERSONA NATURAL SIN NEGOCP  ACUANDO EL DEUDOR HA PRESE1DEUDOR NO EXPUESTO AL RIE0001100010001RESIDENTE EN EL PAIS     PE  PERU           1                           00000BANCO DE LA NACION       0000000001 0041992043  RISCO VEGA ANGELA ROCIO                                                                                                                               0        0000000000000020210820 0000000000000.00   000000000000R01   150141CA DONATELLO 237 PISO 2 DPTO 201 ALT CDRA 6 Y 7 ARTES NORTE           000005243466RISCO                                             VEGA                                                                                                ANGELA                                            ROCIO                                             PE  PERU           198302273140902F3DIVORCIADO     1990874808  ANGELA.RISCOV@GMAIL.COM                                                                         ARISCOV@BN.COM.PE                                                                                                    00015ANALISTA DE SISANALISTA DE SISTEM000000000000000                              0153CARLOS NESTOR  MAURILIA       000000000000                                                                                                                                                                                                                               00000000000000000000                                                                                                                                                                                                                                                                                                                                    000005243466000048 2DONATELLO                                                   237                                                                                                                     2                             201                           CALLE DONATELLO 237 PI 2 DP.INT.201 ALT CDRA 6 Y 7 ARTES NORTE        ALT CDRA 6 Y 7 ARTES NORTE                                                                                               0000000000000000000000000000001150117N00000000                                                                                                                                                      0022052548     150130150141            00000000201710201                                                           000005243466                  0 D           LIMA                                              LIMA                                              SAN BORJA                                         LIMA                                              LIMA                                              SAN BORJA                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  *";
        genericResponse.setData(new TestDatos().getCustomerData(cadena));

        return genericResponse;
    }

    /**
     * =================================================================================================================
     * Búsqueda de cliente por tipo y número de producto
     *
     * @param httpHeaders cabeceras de la aplicación
     * @param request     {@link TypeSearchCustomer}
     * @return {@link PersonaCliente}
     * @throws Throwable error
     */
    @Override
    public GenericResponse<CustomResponse> findCustomerByTypeAndNumberProduct(HttpHeaders httpHeaders, TypeSearchCustomer request) throws Throwable {
    	CustomResponse clienteResponse = new CustomResponse();
        GenericResponse genericResponse=new GenericResponse();
      //  genericResponse.setData(TestDatos.getCustomerData());

        return genericResponse;
    }
    
    /**
     * =================================================================================================================
     * Consultar datos de contactabilidad de cliente
     *
     * @param httpHeaders cabeceras de la aplicación
     * @param request
     * @return
     * @throws Throwable error
     */
    @Override
    public GenericResponse<CustomResponse> findCustomerContactInformationByCic(HttpHeaders httpHeaders,
                                                                  TypeSearchCustomerContactability request) throws
                                                                                                            Throwable
    {
        

        return new GenericResponse<CustomResponse>();
    }
    
    /**
     * =================================================================================================================
     * Búsqueda de cliente por tipo y número de documento
     *
     * @param httpHeaders cabeceras de la aplicación
     * @param request     {@link TypeSearchCustomer}
     * @return {@link PersonaCliente}
     * @throws Throwable error
     */
    @Override
    public GenericResponse<CustomResponse> findCustomerSIDGByTypeAndNumberDocument(HttpHeaders httpHeaders,
                                                                  TypeSearchCustomer request) throws Throwable
    {

        return new GenericResponse<CustomResponse>();
    }

}
