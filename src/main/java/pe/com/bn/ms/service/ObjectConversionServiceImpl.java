package pe.com.bn.ms.service;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.StringUtils;
import org.eclipse.collections.impl.list.Interval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.com.bn.ms.domain.FieldProperties;
import pe.com.bn.ms.domain.PlotParameter;
import pe.com.bn.ms.dto.GatewayRequest;
import pe.com.bn.ms.util.PlotSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static pe.com.bn.ms.util.enums.PlotParameters.CHECK;
import static pe.com.bn.ms.util.enums.PlotParameters.CHECKPOINT_LIST;
import static pe.com.bn.ms.util.enums.PlotParameters.CHECKPOINT_RESPONSE;
import static pe.com.bn.ms.util.enums.PlotParameters.HEADER;
import static pe.com.bn.ms.util.enums.PlotParameters.OBJECT_LIST;
import static pe.com.bn.ms.util.enums.PlotParameters.OBJECT_NESTED;
import static pe.com.bn.ms.util.enums.PlotParameters.OPERATION_CREATE;
import static pe.com.bn.ms.util.enums.PlotParameters.OPERATION_READ;
import static pe.com.bn.ms.util.enums.PlotParameters.OPERATION_UPDATE;
import static pe.com.bn.ms.util.functions.PlotUtil.convertObjectToPlot;
import static pe.com.bn.ms.util.functions.PlotUtil.createPlot;
import static pe.com.bn.ms.util.functions.PlotUtil.listParametersRequest;
import static pe.com.bn.ms.util.functions.PlotUtil.mapToObject;
import static pe.com.bn.ms.util.functions.PlotUtil.validateFieldConstraint;

/**
 * @author prov_destrada
 * @created 22/10/2020 - 06:14 p. m.
 * @project ms-dataclients
 */
@Log4j2
@Service
public class ObjectConversionServiceImpl implements ObjectConversionService
{
    @Autowired private PlotSource plotSource;

    /**
     * =================================================================================================================
     * Convierte un DTO en una trama a partir de un .csv
     *
     * @param plotClass indica la clase que será convertida a trama
     * @param colophon  indica el colofón del archivo .csv (ejem. '_update')
     * @param operation indica el tipo de operación CRUD
     * @param response  {@link GatewayRequest}
     * @return {@link GatewayRequest}
     * @throws Exception error
     */
    @Override
    public <T> GatewayRequest convertDtoToPlot(T plotClass, String colophon, String operation,
                                               GatewayRequest response) throws Throwable
    {

        System.out.println(" convertDtoToPlot colophon:"+colophon+", operation:"+operation+" response:"+response);
        // Comparando los dos mapas
        LinkedHashMap<String, PlotParameter> plotParameters = plotSource
                .fillMap(plotClass, colophon);

        // Extraer los objetos que se encuentran anidados en .csv
        // Estructura: ['nombre de clase', 'nombre de campo']
        List<String[]> nestedObjects = plotParameters
                .entrySet()
                .stream()
                .filter(dataObjectProperties -> dataObjectProperties.getKey().contains(OBJECT_NESTED.getType()))
                .map(nestedObjectName -> {
                    String nestedNameClass = nestedObjectName.getKey();
                    nestedNameClass = nestedNameClass.substring(nestedNameClass.lastIndexOf("-") + 1);

                    String nestedNameClassField = nestedObjectName.getValue().getInput();

                    return new String[]{nestedNameClass, nestedNameClassField};
                })
                .collect(Collectors.toList());

        // Convertir objeto a trama
        LinkedHashMap<String, FieldProperties> plotSourceParameters =
                convertObjectToPlot(plotClass, nestedObjects);

        // Eliminar los tipos anidados
        plotParameters.entrySet()
                .removeIf(r -> r.getKey().contains(OBJECT_NESTED.getType()));

        // ------------------------------------------------------------
        // Validar restricciones de campo
//        if (operation.matches(OPERATION_CREATE.getValue() + "|" + OPERATION_UPDATE.getValue())) {
//            validateFieldConstraint(plotParameters, plotSourceParameters);
//        }
        // ------------------------------------------------------------

        // Extraer las posiciones
        List<Integer> posiciones;
        if (operation.matches(OPERATION_CREATE.getValue())) {
            posiciones = plotParameters.values()
                    .stream()
                    .filter(v -> !v.getType().equals(OBJECT_LIST.getType()))
                    .filter(w -> w.getIn().equals(CHECK.getType()))
                    .map(PlotParameter::getPosition)
                    .collect(Collectors.toList());
        }
        //
        else if (operation.matches(OPERATION_READ.getValue() + "|" + OPERATION_UPDATE.getValue())) {
            posiciones = plotParameters.values()
                    .stream()
                    .filter(w -> w.getIn().equals(CHECK.getType()))
                    .map(PlotParameter::getPosition)
                    .collect(Collectors.toList());
        }
        // Error
        else {
            throw new Exception(" El tipo de operación no existe");
        }

        int min = Collections.min(posiciones);
        int max = Collections.max(posiciones);

        List<Integer> integers = Interval.from(min).to(max);

        // Filtrando solamente los parámetros que son request
        plotParameters.entrySet()
                .removeIf(q -> !integers.contains(q.getValue().getPosition()));

        List<String> listParameters = listParametersRequest(plotParameters, operation);

        // Filtrando solamente los parámetros que son request
        plotSourceParameters.entrySet()
                .removeIf(q -> !listParameters.contains(q.getKey()));

        // Validar que los campos que existen en los objetos, existan en el .csv
        List<String> nonExists = new ArrayList<>();
        plotParameters.forEach((key, value) -> {
            // Crear nuevo objeto
            if (operation.matches(OPERATION_CREATE.getValue())) {
                if (!listParameters.contains(value.getOutput()))
                    nonExists.add(value.getOutput());

            }
            // Leer o actualizar objeto
            else if (operation.matches(OPERATION_READ.getValue() + "|" + OPERATION_UPDATE.getValue())) {
                if (!listParameters.contains(value.getInput()))
                    nonExists.add(value.getInput());
            }
        });

        String plotHeader = createPlot(plotParameters, plotSourceParameters, "HEADER", operation);
        String plotBody   = createPlot(plotParameters, plotSourceParameters, "BODY", operation);

        plotParameters.clear();
        plotSourceParameters.clear();

        response.setDatos(plotHeader.concat(plotBody));

        return response;
    }

    /**
     * =================================================================================================================
     * Convierte una trama a un DTO
     *
     * @param plotClass indica la clase de la cual la trama se convertirá en dto
     * @param plot      trama
     * @param colophon  indica el colofón del archivo .csv (ejem. '_update')
     * @return {@link T}
     * @throws Exception error
     */
    @Override
    public <T> T convertPlotToDto(T plotClass, String plot, String colophon) throws Throwable
    {
        // Descomponer el dto
        LinkedHashMap<String, PlotParameter> dtoFields = plotSource
                .fillMap(plotClass, colophon);

        // ------------------------------------------------------------------
        // Estructura: Este es el objeto principal dto
        // ------------------------------------------------------------------
        //  -> KEY: nombre de la clase del objeto dto
        //  -> VALUE: nombre del objeto lista en el dto, ejem: 'products'
        //     - Posición 0: nombre del campo en 'host' del objeto lista
        //     - Posición 1: nombre del campo del objeto lista
        //     - Posición 2: valor del campo
        LinkedHashMap<String, String[]> mapDtoValues = new LinkedHashMap<>();

        // ------------------------------------------------------------------
        // Estructura: Agrupa los elementos que conforman una lista anidada de un dto
        //             los cuales deben ser ordenados de la siguiente manera:
        // ------------------------------------------------------------------
        //  -> KEY: llave compuesta por el array siguiente:
        //     - Posición 0: nombre de la clase 'dto', ejem: 'Product'
        //     - Posición 1: nombre de la clase 'dto' como campo, ejem: 'products'
        //  -> VALUE: se refiere a 'nestedObjectValue'
        LinkedHashMap<String[], LinkedHashMap<Integer, List<Object[]>>> nestedObject = new LinkedHashMap<>();

        // ----------------------------------------------------------------
        // 1ra parte
        // Setear los campos que no forman parte de listas anidadas al dto
        dtoFields.entrySet()
                .stream()
                .filter(f -> !f.getValue().getType().contains(OBJECT_LIST.getType()))
                .filter(f -> f.getValue().getOut().equals(CHECK.getType()) ||
                             f.getValue().getCheckResponse().matches(CHECK.getType() + "|" +
                                                                     CHECKPOINT_RESPONSE.getType()))
                .forEach(f -> {
                    int beginIndex = f.getValue().getX1() - 1;
                    int endIndex   = f.getValue().getX2() - 1;

                    String field = StringUtils.trim(plot.substring(beginIndex, endIndex));
                    mapDtoValues.put(f.getKey(), new String[]{f.getValue().getOutput(), field,
                                                              f.getValue().getCheckResponse()});
                });

        // ----------------------------------------------------------------
        // 2da parte

        // Verificar que existen listas
        boolean existList = dtoFields.entrySet()
                .stream()
                .anyMatch(a -> a.getValue().getType().contains(OBJECT_LIST.getType()));

        if (existList) {

            // Extraer tamaño de la lista
            int[] positions = dtoFields.values()
                    .stream()
                    .filter(f -> f.getCheckResponse().equals(CHECKPOINT_LIST.getType()))
                    .map(m -> new int[]{m.getX1() - 1, m.getX2() - 1})
                    .findFirst()
                    .orElseGet(() -> new int[]{0, 0});

            int sizeList = Integer.parseInt(plot.substring(positions[0], positions[1]));

            // Filtrar los campos que conforman el dto anidado
            Map<String, PlotParameter> nestedDtoProperties = dtoFields.entrySet()
                    .stream()
                    .filter(f -> f.getValue().getType().contains(OBJECT_LIST.getType()))
                    .filter(f -> f.getValue().getOut().equals(CHECK.getType()))
                    .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

            // Cargar la lista

            // ------------------------------------------------------------------
            // Estructura: Agrupa los elementos value de 'nestedObject'
            // ------------------------------------------------------------------
            //  -> KEY: índice correlativo respecto al tamaño de la lista 'sizeList'
            //  -> VALUE: elementos
            //     - Posición 0: nombre del campo del objeto lista
            //     - Posición 1: valor del campo
            LinkedHashMap<Integer, List<Object[]>> nestedObjectValue = new LinkedHashMap<>();

            // Extraer el nombre del dto, su nombre anidado y la longitud de la trama
            Object[] nestedPropertiesDto = dtoFields.values()
                    .stream()
                    .filter(f -> f.getCheckResponse().equals("0"))
                    .map(m -> new Object[]{m.getType().substring(m.getType().lastIndexOf("_") + 1),
                                           m.getOutput(), m.getLongitud()})
                    .findFirst()
                    .orElseGet(() -> new Object[]{"", ""});

            // Setear los valores de los campos del dto anidado
            for (int i = 0; i < sizeList; i++) {
                List<Object[]> fieldNestedValues = new ArrayList<>();

                int item = i;
                nestedDtoProperties.forEach((key, value) -> {

                    int sizeNestedPlot = Integer.parseInt(nestedPropertiesDto[2].toString());

                    int x1 = value.getX1() + sizeNestedPlot * item - 1;
                    int x2 = value.getX2() + sizeNestedPlot * item - 1;

                    String valueField = plot.substring(x1, x2);
                    fieldNestedValues.add(new String[]{value.getOutput(), valueField});
                });
                nestedObjectValue.put(i, fieldNestedValues);
            }

            // Extraer el nombre del dto, su nombre anidado
            String[] nestedDto = new String[]{nestedPropertiesDto[0].toString(),
                                              nestedPropertiesDto[1].toString()};

            nestedObject.put(nestedDto, nestedObjectValue);
        }

        return mapToObject(plotClass, mapDtoValues, nestedObject);
    }

    /**
     * =================================================================================================================
     * Validar headers de una trama, verificar si la transacción ha sido correcta
     *
     * @param plotClass indica la clase que será convertida a trama
     * @param plot      trama
     * @param colophon  indica el colofón del archivo .csv (ejem. '_update')
     * @param <T>       objeto genérico
     * @return
     * @throws Throwable error
     */
    @Override
    public <T> T validateHeadersForPlot(T plotClass, String plot, String colophon) throws Throwable
    {
        // Descomponer el dto
        LinkedHashMap<String, PlotParameter> dtoFields = plotSource
                .fillMap(plotClass, colophon);

        LinkedHashMap<String, String[]> mapDtoValues = new LinkedHashMap<>();

        // Extraer las posiciones de los mensajes
        // ----------------------------------------------------------------
        // 1ra parte
        // Extraer los mensajes que retorna HOST
        dtoFields.entrySet()
                .stream()
                .filter(f -> f.getValue().getType().contains(HEADER.getType()))
                .filter(f -> f.getValue().getCheckResponse().equals(CHECKPOINT_RESPONSE.getType()))
                .forEach(w -> {
                    int beginIndex = w.getValue().getX1() - 1;
                    int endIndex   = w.getValue().getX2() - 1;

                    String field = StringUtils.trim(plot.substring(beginIndex, endIndex));
                    mapDtoValues.put(w.getKey(), new String[]{w.getValue().getOutput(), field,
                                                              w.getValue().getCheckResponse()});
                });

        return mapToObject(plotClass, mapDtoValues, new LinkedHashMap<>());
    }
}
