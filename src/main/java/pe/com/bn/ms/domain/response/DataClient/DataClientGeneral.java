package pe.com.bn.ms.domain.response.DataClient;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.com.bn.ms.domain.GenericResponse;
import pe.com.bn.ms.domain.ParameterGroup;
import io.swagger.annotations.ApiModelProperty;
import pe.com.bn.ms.util.enums.Messages;

import java.io.Serializable;

@ApiModel(description = "Response de clientes en la busqueda")
@Data

public class DataClientGeneral implements Serializable{
	@ApiModelProperty(value = "")	private String	cinterno_cic;
	@ApiModelProperty(value = "")	private String	officeInputCode;
	@ApiModelProperty(value = "")	private String	officeOperationalCode;
	@ApiModelProperty(value = "")	private String	admissionBankDate;
	@ApiModelProperty(value = "")	private String	businessExecutiveCode;
	@ApiModelProperty(value = "")	private String	businessExecutiveDesc;
	@ApiModelProperty(value = "")	private String	economicSectorCode;
	@ApiModelProperty(value = "")	private String	economicActivityCode;
	@ApiModelProperty(value = "")	private String	economicActivityDesc;
	@ApiModelProperty(value = "")	private String	clientType;
	@ApiModelProperty(value = "")	private String	clientTypeDesc;
	@ApiModelProperty(value = "")	private String	bankingType;
	@ApiModelProperty(value = "")	private String	bankingTypeDesc;
	@ApiModelProperty(value = "")	private String	segmentClientCode;
	@ApiModelProperty(value = "")	private String	segmentClientDesc;
	@ApiModelProperty(value = "")	private String	documentType;
	@ApiModelProperty(value = "")	private String	documentTypeDesc;
	@ApiModelProperty(value = "")	private String	documentNumber;
	@ApiModelProperty(value = "")	private String	fullName;
	@ApiModelProperty(value = "")	private String	personType;
	@ApiModelProperty(value = "")	private String	personTypeDesc;
	@ApiModelProperty(value = "")	private String	prefWorkingPhone;
	@ApiModelProperty(value = "")	private String	workingPhone;
	@ApiModelProperty(value = "")	private String	annexPhone;
	@ApiModelProperty(value = "")	private String	prefHomePhone;
	@ApiModelProperty(value = "")	private String	homePhone;
	@ApiModelProperty(value = "")	private String	BCRAgentCode;
	@ApiModelProperty(value = "")	private String	SBSClientCode;
	@ApiModelProperty(value = "")	private String	SBSJointCode;
	@ApiModelProperty(value = "")   private String	debtorClasBNCode;
	@ApiModelProperty(value = "")	private String	debtorClasSBSCode;
	@ApiModelProperty(value = "")	private String	debtorClasSBSDesc;
	@ApiModelProperty(value = "")	private String	clientMagnitudeCode;
	@ApiModelProperty(value = "")	private String	clientMagnitudeDesc;
	@ApiModelProperty(value = "")	private String	flagSupplementaryData;
	@ApiModelProperty(value = "")	private String	relLabBcoCode;
	@ApiModelProperty(value = "")	private String	accionistBcoCode;
	@ApiModelProperty(value = "")	private String	atrasoDeuCode;
	@ApiModelProperty(value = "")	private String	atrasoDeuDesc;
	@ApiModelProperty(value = "")	private String	riesgCambCode;
	@ApiModelProperty(value = "")	private String	riesgCambDesc;
	@ApiModelProperty(value = "")	private String	flagCheqSinFondoBco;
	@ApiModelProperty(value = "")	private String	flagCheqSinFondoSBS;
	@ApiModelProperty(value = "")	private String	flagLavadoDinero;
	@ApiModelProperty(value = "")	private String	flagInternetKey;
	@ApiModelProperty(value = "")	private String	flagMultiredCard;
	@ApiModelProperty(value = "")	private String	flagoMultiredLoan;
	@ApiModelProperty(value = "")	private String	flagMultiredGuarantor;
	@ApiModelProperty(value = "")	private String	flagNationality;
	@ApiModelProperty(value = "")	private String	flagAcceptor;
	@ApiModelProperty(value = "")	private String	flagExternalTrade;
	@ApiModelProperty(value = "")	private String	flagCountryResidence;
	@ApiModelProperty(value = "")	private String	DescCountryResidence;
	@ApiModelProperty(value = "")	private String	codeCountryOfResidence;
	@ApiModelProperty(value = "")	private String	descCountryOfResidence;
	@ApiModelProperty(value = "")	private String	codePublicSector;
	@ApiModelProperty(value = "")	private String	descPublicSector;
	@ApiModelProperty(value = "")	private String	flagDead;
	@ApiModelProperty(value = "")	private String	codeEntityClient;
	@ApiModelProperty(value = "")	private String	descEntityClient;
	@ApiModelProperty(value = "")	private String	internalClientCode;
	@ApiModelProperty(value = "")	private String	internalGroupCode;
	@ApiModelProperty(value = "")	private String	documentTypeReport;
	@ApiModelProperty(value = "")	private String	documentNumberReport;
	@ApiModelProperty(value = "")	private String	fullNameReport;
	@ApiModelProperty(value = "")	private String	flagInterdicto;
	@ApiModelProperty(value = "")	private String	lastLavadoActivoDate;
	@ApiModelProperty(value = "")	private String	lastClientCode;
	@ApiModelProperty(value = "")	private String	lastGroupCode;
	@ApiModelProperty(value = "")	private String	lastChangeDate;
	@ApiModelProperty(value = "")	private String	annualProfit;
	@ApiModelProperty(value = "")	private String	clasifDebtorCode;
	@ApiModelProperty(value = "")	private String	clasifDebtorAlignedCode;
	@ApiModelProperty(value = "")	private String	docComplementaryType;
	@ApiModelProperty(value = "")	private String	docComplementaryNum;
	@ApiModelProperty(value = "")	private String	openingClientType;
	@ApiModelProperty(value = "")	private String	flagPEP;
	@ApiModelProperty(value = "")	private String	flagContractual;
	@ApiModelProperty(value = "")	private String	flagTextMessage;
	@ApiModelProperty(value = "")	private String	flagForcedToInform;
	@ApiModelProperty(value = "")	private String	lifeCycleVidaClientState;
	@ApiModelProperty(value = "")	private String	creationReasonCode;
	@ApiModelProperty(value = "")	private String	economicSituationCode;
	@ApiModelProperty(value = "")	private String	firstAgreementDate;
	@ApiModelProperty(value = "")	private String	flagDataProtected;
	@ApiModelProperty(value = "")	private String	flagPublicity;
	@ApiModelProperty(value = "")	private String	flagCesionOthers;
	@ApiModelProperty(value = "")	private String	flagRestrictedAccess;
	@ApiModelProperty(value = "")	private String	flagVIP;
	@ApiModelProperty(value = "")	private String	flagBlackList;
	@ApiModelProperty(value = "")	private String	flagSpecialTrat;
	@ApiModelProperty(value = "")	private String	flagClteBlock;
	@ApiModelProperty(value = "")	private String	flagToken;
	@ApiModelProperty(value = "")	private String	flagBankMovil;
	@ApiModelProperty(value = "")	private String	UbigeoCodeAddressClte;
	@ApiModelProperty(value = "")	private String	DescAddressClte;

	public DataClientGeneral(String cinterno_cic) {
		super();
		this.cinterno_cic = cinterno_cic;
	}
}
