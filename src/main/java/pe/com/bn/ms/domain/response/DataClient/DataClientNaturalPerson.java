package pe.com.bn.ms.domain.response.DataClient;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel(description = "Response de clientes natural")
@Data

public class DataClientNaturalPerson implements Serializable{
	@ApiModelProperty(value = "") private String lastName01;
	@ApiModelProperty(value = "") private String lastName02;
	@ApiModelProperty(value = "") private String marriedName;
	@ApiModelProperty(value = "") private String firstName;
	@ApiModelProperty(value = "") private String secondName;
	@ApiModelProperty(value = "") private String countryOfBirthCode;
	@ApiModelProperty(value = "") private String countryOfBirthDesc;
	@ApiModelProperty(value = "") private String birthDate;
	@ApiModelProperty(value = "") private String ubigeoBirthType;
	@ApiModelProperty(value = "") private String ubigeoBirthCode;
	@ApiModelProperty(value = "") private String sexCode;
	@ApiModelProperty(value = "") private String civilStatusCode;
	@ApiModelProperty(value = "") private String civilStatusDesc;
	@ApiModelProperty(value = "") private String codOperadorMobile;
	@ApiModelProperty(value = "") private String numberMobile;
	@ApiModelProperty(value = "") private String personalEmail;
	@ApiModelProperty(value = "") private String workEmail;
	@ApiModelProperty(value = "") private String degreeOfInstructionCode;
	@ApiModelProperty(value = "") private String professionCode;
	@ApiModelProperty(value = "") private String professionDesc;
	@ApiModelProperty(value = "") private String occupationCode;
	@ApiModelProperty(value = "") private String occupationDesc;
	@ApiModelProperty(value = "") private String occupationCodeReported;
	@ApiModelProperty(value = "") private String flagBusinessPerson;
	@ApiModelProperty(value = "") private String numberRUC;
	@ApiModelProperty(value = "") private String dependentNum;
	@ApiModelProperty(value = "") private String localityBirth;
	@ApiModelProperty(value = "") private String yearOfStudy;
	@ApiModelProperty(value = "") private String heightClient;
	@ApiModelProperty(value = "") private String fatherName;
	@ApiModelProperty(value = "") private String motherName;

}
