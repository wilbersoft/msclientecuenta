package pe.com.bn.ms.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
@ToString
public class PlotParameter
{
    private int     position;           // Posición del campo
    private String  type;               // Indica si el campo es cabecera, body o lista
    private String  typeFieldHost;      // Tipo de campo, numérico, string, etc
    private String  nameHost;           // Nombre del campo en HOST
    private String  in;                 // Check de campos input
    private String  input;              // Nombre de campo input
    private String  out;                // Check de campos output
    private String  output;             // Nombre de campo output
    private Integer longitud;           // Tamaño del campo
    private Integer x1;                 // Posición inicial en la trama
    private Integer x2;                 // Posición final en la trama
    private String  checkResponse;      //
    private String  checkPoint;         //

    // Constructores

    public PlotParameter(String position, String type, String typeFieldHost, String in, String input,
                         String out, String output, String longitud, String x1, String x2, String checkResponse,
                         String checkPoint)
    {
        this.position      = Integer.parseInt(position);
        this.type          = type;
        this.typeFieldHost = typeFieldHost;
        this.in            = in;
        this.input         = input;
        this.out           = out;
        this.output        = output;
        this.longitud      = StringUtils.isNotBlank(longitud) ? Integer.valueOf(longitud) : null;
        this.x1            = StringUtils.isNotBlank(x1) ? Integer.valueOf(x1) : null;
        this.x2            = StringUtils.isNotBlank(x2) ? Integer.valueOf(x2) : null;
        this.checkResponse = checkResponse;
        this.checkPoint    = checkPoint;
    }

}
