package pe.com.bn.ms.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ParameterRequest implements Serializable
{
    private String sistema;
    private String cuenta;
    private String idUsuario;
    private byte[] clave;
}
