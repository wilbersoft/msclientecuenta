package pe.com.bn.ms.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class FieldProperties
{
    private String   nameField;
    private Integer  lengthField;
    private Class<?> typeField;
    private String   valueField;

    public FieldProperties(String nameField, Integer lengthField, Class<?> typeField, Object valueField)
    {
        this.nameField   = nameField;
        this.lengthField = lengthField;
        this.typeField   = typeField;
        this.valueField  = valueField.toString();
    }
}
