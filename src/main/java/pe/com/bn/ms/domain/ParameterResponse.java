package pe.com.bn.ms.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ParameterResponse implements Serializable
{
    private String           aliasSistema;
    private String           codigoRetorno;
    private String           mensaje;
    private ParameterGroup[] grupoParametro;
}
