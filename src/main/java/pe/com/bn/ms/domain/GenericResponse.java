package pe.com.bn.ms.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pe.com.bn.ms.util.enums.Messages;

import java.io.Serializable;

@ApiModel(description = "Wrapper genérico por el cual pasarán todos objetos respuesta de las api")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenericResponse<T> implements Serializable
{
	
    @ApiModelProperty(value = "Código de resultado de la consulta",
                      notes = "Es el código de respuesta que retorna despues de realizar una determinada operación",
                      required = true, example = "00000")
    private String codResult;

    @ApiModelProperty(value = "Objeto de resultado de la consulta",
                      notes = "Es el objeto de respuesta que retorna despues de realizar una determinada operación")
    private T data;

    @ApiModelProperty(value = "Mensaje de resultado de la consulta",
                      notes = "Es el mensaje de respuesta que retorna despues de realizar una determinada operación, " +
                              "este mensaje es el que será visible para el cliente final",
                      example = "Consulta existosa")
    private String msg;

    @ApiModelProperty(value = "Mensaje interno de resultado de la consulta",
                      notes = "Es el mensaje interno de respuesta que retorna despues de realizar una determinada operación, " +
                              "este mensaje es de control interno del banco.",
                      example = "La consulta a base de datos fue exitosa")
    private String msgError;

    public GenericResponse(String codResult, T data)
    {
        this.codResult = codResult;
        this.data      = data;
    }

    public GenericResponse(T data)
    {
        this.codResult = Messages.SUCCESSFUL.getCodResult();
        this.msg       = Messages.SUCCESSFUL.getMsg();
        this.msgError  = Messages.SUCCESSFUL.getMsgError();
        this.data      = data;
    }
}
