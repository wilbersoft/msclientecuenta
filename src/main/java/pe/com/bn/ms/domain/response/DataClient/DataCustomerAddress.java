package pe.com.bn.ms.domain.response.DataClient;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@ApiModel(description = "Response de clientes direccion")
@Data

public class DataCustomerAddress implements Serializable{

}
