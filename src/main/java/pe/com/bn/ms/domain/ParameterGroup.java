package pe.com.bn.ms.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ParameterGroup implements Serializable
{
    protected String      aliasGrupo;
    protected String      aliasDescripGrupo;
    protected String      tipoGrupo;
    protected Parameter[] parametro;
}
