package pe.com.bn.ms.domain.response.DataClient;

import java.io.Serializable;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import pe.com.bn.ms.Application;
import pe.com.bn.ms.util.Shared.DataUtil;
@Data

//Custom response general para invocar a los metodos dentro de la API

@ApiModel(description = "Datos Principales del Cliente")
public class CustomResponse implements Serializable{
	private DataClientGeneral clientGeneral;
	private DataClientIndicators clientInidicators;
	private DataClientLegalEntity clientLegalEntity;
	private DataClientNaturalPerson clientNaturalPerson;
	private DataCustomerAddress clientAddress;
	@ApiModelProperty(value = "Entidad de consulta de Cliente") private String entidadResultadoDatos;
	Application trama = new Application();
	String resultInput = trama.obtenerDatosEntrada();
	String resultOutput = trama.obtenerDatosSalida();
}
