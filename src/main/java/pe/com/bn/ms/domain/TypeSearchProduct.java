package pe.com.bn.ms.domain;

import org.apache.commons.lang3.StringUtils;

import lombok.Data;

/**
 * @author prov_destrada
 * @created 23/10/2020 - 11:53 a. m.
 * @project ms-dataclients
 */
@Data
public class TypeSearchProduct
{
	private String typeSearch;
    private String cic;
    private String typeDocument;
    private String numberDocument;
    private String typeProduct;
    private String numProduct;
    private String userApplication;
    
    public TypeSearchProduct(String typeSearch, String cic, String typeDocument, String numberDocument,
            String typeProduct, String numberProduct, String userApplication)
    {
    	this.typeSearch         = StringUtils.isNotBlank(typeSearch) ? typeSearch : null;
    	this.cic                = StringUtils.isNotBlank(cic) ? cic : null;
    	this.typeDocument       = StringUtils.isNotBlank(typeDocument) ? typeDocument : null;
    	this.numberDocument     = StringUtils.isNotBlank(numberDocument) ? numberDocument : null;
    	this.typeProduct        = StringUtils.isNotBlank(typeProduct) ? typeProduct : null;
    	this.numProduct      = StringUtils.isNotBlank(numberProduct) ? numberProduct : null;
		this.userApplication = StringUtils.isNotBlank(userApplication) ? userApplication : null;
    }
}
