package pe.com.bn.ms.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Parameter implements Serializable
{
    protected String aliasParam;
    protected String descripcionParam;
    protected String campoParam;
    protected String valorParam;
    protected String tipoParam;
}
