package pe.com.bn.ms.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * SERVICE_UNAVAILABLE - 503
 * Lanza una excepcion controlada cuando no se puede comunicar con un servicio
 */
@Getter
@NoArgsConstructor
@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class ServiceNotAvailableEx extends RuntimeException
{
    private String codResult;
    private String msg;
    private String msgError;

    public ServiceNotAvailableEx(String codResult, String msgError, String msg)
    {
        super(msgError);
        this.codResult = codResult;
        this.msg       = msg;
        this.msgError  = msgError;
    }

    public ServiceNotAvailableEx(String msgError, String msg)
    {
        super(msgError);
        this.msg      = msg;
        this.msgError = msgError;
    }

    public ServiceNotAvailableEx(String msgError)
    {
        super(msgError);
        this.msgError = msgError;
    }
}
