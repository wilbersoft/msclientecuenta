package pe.com.bn.ms.exception.base4XX;

import lombok.Getter;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static pe.com.bn.ms.util.enums.MessagesError.HANDLE_NOT_FOUND;

/**
 * @author prov_destrada
 * @description Contiene los elementos base de una excepción
 */
@Getter
public class ResourceNotFoundException extends RuntimeException
{
    private final String codeResult;
    private final String msg;
    private final String msgError;
    private       String parameters;

    public ResourceNotFoundException()
    {
        this.codeResult = HANDLE_NOT_FOUND.getCodResult();
        this.msg        = HANDLE_NOT_FOUND.getMsg();
        this.msgError   = HANDLE_NOT_FOUND.getMsgError();
    }

    public ResourceNotFoundException(String parameters)
    {
        this.codeResult = HANDLE_NOT_FOUND.getCodResult();
        this.msg        = HANDLE_NOT_FOUND.getMsg();
        this.msgError   = HANDLE_NOT_FOUND.getMsgError();
        this.parameters = parameters;
    }

    public ResourceNotFoundException(String msg, String msgError, String parameters)
    {
        this.codeResult = HANDLE_NOT_FOUND.getCodResult();
        this.msg        = msg;
        this.msgError   = isBlank(msgError) ? msg : msgError;
        this.parameters = parameters;
    }

    public ResourceNotFoundException(Throwable cause, String msg, String parameters)
    {
        super(cause);
        this.codeResult = HANDLE_NOT_FOUND.getCodResult();
        this.msg        = msg;
        this.msgError   = msg;
        this.parameters = parameters;
    }

}
