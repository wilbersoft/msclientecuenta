package pe.com.bn.ms.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * INTERNAL_SERVER_ERROR - 500
 * Este error se genera cuando falla la conexión a base de datos
 */
@Getter
@NoArgsConstructor
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class DatasourceException extends RuntimeException
{
    private String codResult;
    private String msg;
    private String msgError;

    public DatasourceException(String codResult, String msgError, String msg)
    {
        super(msgError);
        this.codResult = codResult;
        this.msg       = msg;
        this.msgError  = msgError;
    }

    public DatasourceException(String msgError, String msg)
    {
        super(msgError);
        this.msg      = msg;
        this.msgError = msgError;
    }

    public DatasourceException(String msgError)
    {
        super(msgError);
        this.msgError = msgError;
    }
}
