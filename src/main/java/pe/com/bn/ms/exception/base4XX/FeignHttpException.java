package pe.com.bn.ms.exception.base4XX;

import lombok.Getter;
import pe.com.bn.ms.dto.ErrorResponse;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static pe.com.bn.ms.util.enums.MessagesError.HANDLE_CLIENT;

/**
 * @author prov_destrada
 * @created 11/09/2020 - 05:15 p.m.
 * @project MSSofTokenValidate
 */
@Getter
public class FeignHttpException extends RuntimeException
{
    private final String        errorCode;
    private final String        friendlyMessage;
    private final String        technicalMessage;
    private       Object        parameters;
    private       ErrorResponse error;

    public FeignHttpException()
    {
        this.errorCode        = HANDLE_CLIENT.getCodResult();
        this.friendlyMessage  = HANDLE_CLIENT.getMsg();
        this.technicalMessage = HANDLE_CLIENT.getMsgError();
    }

    public FeignHttpException(ErrorResponse error)
    {
        this.errorCode        = HANDLE_CLIENT.getCodResult();
        this.friendlyMessage  = HANDLE_CLIENT.getMsg();
        this.technicalMessage = HANDLE_CLIENT.getMsgError();
        this.error            = error;
    }

    public FeignHttpException(Object parameters)
    {
        this.errorCode        = HANDLE_CLIENT.getCodResult();
        this.friendlyMessage  = HANDLE_CLIENT.getMsg();
        this.technicalMessage = HANDLE_CLIENT.getMsgError();
        this.parameters       = parameters;
    }

    public FeignHttpException(String friendlyMessage, String technicalMessage, Object parameters)
    {
        this.errorCode        = HANDLE_CLIENT.getCodResult();
        this.friendlyMessage  = friendlyMessage;
        this.technicalMessage = isBlank(technicalMessage) ? friendlyMessage : technicalMessage;
        this.parameters       = parameters;
    }

    public FeignHttpException(Throwable cause, String friendlyMessage, Object parameters)
    {
        super(cause);
        this.errorCode        = HANDLE_CLIENT.getCodResult();
        this.friendlyMessage  = friendlyMessage;
        this.technicalMessage = friendlyMessage;
        this.parameters       = parameters;
    }

}
