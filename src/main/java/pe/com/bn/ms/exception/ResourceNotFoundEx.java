package pe.com.bn.ms.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * BAD_REQUEST - 400
 * Dispara un error cuando la solicitud no produce ningún resultado
 */
@Getter
@NoArgsConstructor
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ResourceNotFoundEx extends RuntimeException
{
    private String codResult;
    private String msg;
    private String msgError;

    public ResourceNotFoundEx(String codResult, String msgError, String msg)
    {
        super(msgError);
        this.codResult = codResult;
        this.msg       = msg;
        this.msgError  = msgError;
    }

    public ResourceNotFoundEx(String msgError, String msg)
    {
        super(msgError);
        this.msg      = msg;
        this.msgError = msgError;
    }

    public ResourceNotFoundEx(String msgError)
    {
        super(msgError);
        this.msgError = msgError;
    }
}
