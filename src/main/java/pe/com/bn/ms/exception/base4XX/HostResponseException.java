package pe.com.bn.ms.exception.base4XX;

import lombok.Getter;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static pe.com.bn.ms.util.enums.MessagesError.HANDLE_HOST;

/**
 * @author prov_destrada
 * @description Contiene los elementos base de una excepción
 */
@Getter
public class HostResponseException extends RuntimeException
{
    private final String errorCode;
    private final String friendlyMessage;
    private final String technicalMessage;
    private       Object parameters;

    public HostResponseException()
    {
        this.errorCode        = HANDLE_HOST.getCodResult();
        this.friendlyMessage  = HANDLE_HOST.getMsg();
        this.technicalMessage = HANDLE_HOST.getMsgError();
    }

    public HostResponseException(String parameters)
    {
        this.errorCode        = HANDLE_HOST.getCodResult();
        this.friendlyMessage  = HANDLE_HOST.getMsg();
        this.technicalMessage = HANDLE_HOST.getMsgError();
        this.parameters       = parameters;
    }

    public HostResponseException(String friendlyMessage, String technicalMessage, Object parameters)
    {
        this.errorCode        = HANDLE_HOST.getCodResult();
        this.friendlyMessage  = friendlyMessage;
        this.technicalMessage = isBlank(technicalMessage) ? friendlyMessage : technicalMessage;
        this.parameters       = parameters;
    }

    public HostResponseException(Throwable cause, String friendlyMessage, Object parameters)
    {
        super(cause);
        this.errorCode        = HANDLE_HOST.getCodResult();
        this.friendlyMessage  = friendlyMessage;
        this.technicalMessage = friendlyMessage;
        this.parameters       = parameters;
    }

}
