package pe.com.bn.ms.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pe.com.bn.ms.domain.GenericResponse;

/**
 * OK - 200
 * Cuando ocurre una excepción en un endpoint, solo pivotea el error
 * para mostrarlo en un nivel superior
 */
@Getter
@AllArgsConstructor
public class EndpointException extends RuntimeException
{
    private final GenericResponse<?> genericResponse;
}
